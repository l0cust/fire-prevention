# fire-prevention

This is the fire protection activities managing project. It is composed of two parts. The first one is the server which is a REST API for managing data from MOngoDB database. The second part is the front-end written in Angular and it is an actual application.

## Documentation

There is only REST [API](server/docs/api.md) documentation and it is only for further development.

## Starting the application

### Server

It needs following enviroment variables to run:
* database_url=mongodb://localhost:27017
* db_user=root
* db_pass=example
* db_name=fire_prevention
* name_app=fire-prevention - not needed
* secret_jwt=ea<wjL\J&e5]?hpZ88n=j6<e@.N+\Q&w?g
* bucket_endpoint=ams3.digitaloceanspaces.com
* bucket_key_id=id_of_the_bucket
* bucket_key_secret=here_the_secret
* bucket_name=digitalsolutions-sei

Next you enter following commands on development enviromnment:

```
npm install
npm run start:livereload
```

### Front-end

No environment variables required. Just enter:

```
npm install
npm start
```
