# Dokumentacja API serwera projektu fire-prevention-server.

Jest to serwer projektu fire-prevention obsługujacy RESTowe API. Dane przesyłane są w formacie JSON pomiędzy aplikacją front-endową w angularze a node.js'owym back-endem napisanym przy pomocy frameworka ExpressJS. Transmisja danych przy autentykacji jest nieszyfrowana (dane są przesyłane otwartym tekstem).

Obecna dokumentacja to szczegółowy opis zapytań URL oraz opis poszczególnych komponentów po stronie serwera obsługujących te zapytania. Przy każdym zapytaniu jest wklejona lista nagłówków oraz zawartość wysyłana przez aplikację angularową oraz zawartość odpowiedzi serwera.

## Opis URLi

### Logowanie

```
/api/auth/signin
```

URL używany do logowania użytkowników i weryfikacji tożsamości. Wywoływany jest dwukrotnie. Pierwszy raz zapytanie jest wysyłane metodą OPTIONS w celu ustawienia metody i nagłówka odpowiedzi oraz drugi raz metodą POST z danymi z formularza logowania.

Pierwsze wywołanie, metoda OPTIONS, wysyła tylko nagłówki:

    Access-Control-Request-Method: POST
    Origin: http://localhost:4200
    Referer: http://localhost:4200/
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    DNT: 1
    Access-Control-Request-Headers: content-type

Odpowiedź serwera - status 204 (No Content):

    Date: Sat, 11 May 2019 07:27:44 GMT
    X-Powered-By: Express
    Vary: Access-Control-Request-Headers
    Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE
    Access-Control-Allow-Origin: *
    Connection: keep-alive
    Access-Control-Allow-Headers: content-type
    Content-Length: 0

Drugie wywołanie - metoda POST, wysyła dane logowania:

    Accept: application/json, text/plain, */*
    Referer: http://localhost:4200/
    Origin: http://localhost:4200
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    DNT: 1
    Content-Type: application/json

    {"user":{"email":"mch.json@gmail.com","password":"kunacobar0sc0fp"}}

Odpowiedź serwera - status 200 (OK) i dane zalogowanego użytkownika:

    Access-Control-Allow-Origin: *
    Date: Sat, 11 May 2019 07:27:44 GMT
    Connection: keep-alive
    X-Powered-By: Express
    ETag: W/"15f-Rsw/rIZs+6uKK7tbdtpE1z6L3a0"
    Content-Length: 351
    Content-Type: application/json; charset=utf-8

    {"user":{"_id":"5cd1cd929eddc7bd04032876","name":"Artur","email":"mch.json@gmail.com","role":"employee","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1jaC5qc29uQGdtYWlsLmNvbSIsIl9pZCI6IjVjZDFjZDkyOWVkZGM3YmQwNDAzMjg3NiIsInJvbGUiOiJlbXBsb3llZSIsImV4cCI6MTU1NzY0NjA2NCwiaWF0IjoxNTU3NTU5NjY0fQ.vFlZDvGvw0DeOMCj3wSagoiJ25PEDeaXDAGr-DovA48"}}

Adres "http://localhost:4200" to przykładowy adres aplikacji angularowej.

Dane logowania wysyłane w drugim wywołaniu żądania URL są raczej jasne i nie wymagają wyjaśnień. Dane JSON wysyłane w odpowiedzi wymagają drobnego wyjaśnienia.

* "name" - nazwa lub imię użytkownika - pole wymagane przy dodawaniu użytkownika do bazy danych,
* "email" - e-mail, przy pomocy którego użytkownik loguje się do aplikacji,
* "role" - rola użytkownika aplikacji (pracownik - employee lub administrator - admin),
* "_id" - id użytkownika w bazie danych - generowane automatycznie przy dodawaniu dokumentów do kolekcji,
* "token" - token zalogowanego użytkownika wygenerowany przez json-web-token.

Żądanie jest obsługiwane przez klasę AuthRouter zdefiniowaną w pliku `fire-prevention-server/src/routes/auth.ts`. Klasa podpinana jest przez metodę routes() w klasie Server w pliku `fire-prevention-server/src/server.ts`.

AuthRouter obsługuje to zapytanie poprzez metodę signIn() która podpinana jest przez metodę init() definiującą jaki adres URL ma być obsługiwany przez jakie funkcje. Metoda signIn() jest zdefiniowana tak, jak każda funkcja będąca middleware'em obsługującym żądanie URL we frameworku Express. Taka funkcja musi przyjmować trzy parametry (lub dwa jeśli jest to ostatnia funkcja w serii middleware'ów): request (będący obiektem typu express.Request), response (będący obiektem typu express.Response) oraz next (parametr typu Function do którego jest przypisany express.NextFunction). Funkcja next() jest używana do przekazania kontroli następnemu middleware'owi po zakończeniu własnych operacji procesujących żądanie. W przypadku middleware'a przyjmującego dwa parametry przyjmuje on tylko request i response. Metoda signIn() jest zdefiniowana jako async ze względu na obsługę bazy danych przy pomocy promise'ów.

Przed autentykacją metoda signIn() sprawdza poprawność wypełnionych pól, a dokładniej czy żadne z pól nie jest puste. Następnie proces autentykacji kierowany jest do biblioteki passport poprzez metodę authenticate(), która otrzymuje dwa lub trzy parametry: nazwa_używanej_strategii (w tym przypadku "local"), opcje (JSON z dodatkowymi opcjami dotyczącymi przechowywania danych użytkownika w sesji lub zachowania się metody po zweryfikowaniu użytkownika) oraz funkcję przyjmującą trzy parametry (err, user, info). Drugi i trzeci parametr funkcji passport.authenticate() są opcjonalne co oznacza, że możemy przykładowo pominąć dodatkowe opcje i zamiast nich użyć wspomnianej funkcji. Parametry funkcji oznaczają odpowiednio błąd (wyłowany przez wyrzucenie wyjątku lub inny błąd w kodzie), użytkownik (obiekt przechowujący dane użytkownika zweryfikowanego lub zmienna typu null jeśli weryfikacja się nie powiodła) oraz informacyjny obiekt z wiadomościami o błędzie. W przypadku fire-prevention-server użyte są wszystkie trzy parametry funkcji authenticate. Drugi zawiera tylko ustawienie wyłączania sesji, a trzecim jest właśnie omówiona funkcja.

Biblioteka passport używa pluginu zwanego strategią. W tym przypadku jest to LocalStrategy z pakietu passport-local. Konfiguracja tej strategii i zarazem biblioteki passport znajduje się w klasie ConfigPassport w pliku `fire-prevention-server/src/auth/config-passport.ts`.

Konfiguracja wykonywana jest przez metodę init(), która jest wywoływana w konstruktorze klasy. Przy inicjalizacji LocalStrategy do konstruktora są przekazywane dwa parametry: konfiguracja pól z formularza logowania oraz funkcja weryfikująca przyjmująca za parametry nazwę użytkownika, hasło i funcję done(). Funkcja weryfikująca pobiera użytkownika o odpowiednim e-mailu z bazy danych. W przypadku jeśli użytkownika takiego nie ma w bazie danych lub jego hasło jest niepoprawne poprzez funkcję done() zwracane są trzy parametry: null (ponieważ nie ma błędu ani wyjątku), false (lub jeśli weryfikacja powiodła się to obiekt z użytkownikiem), oraz obiekt z informacją o błędzie. W przypadku powodzenia weryfikacji trzeci parametr jest pomijany. W przypadku błędu przesyłany jest tylko pierwszy parametr z informacją o błędzie.

Dane z bazy danych pobierane są przez klasę Database zdefiniowaną w pliku `fire-prevention-server/src/models/database.ts`. Jest to klasa odpowiedzialna za konfigurację i nawiązanie połączenia z bazą danych. Dodatkowo w konstruktorze inicjowane są modele typów reprezentowanych przez interfejsy definiujące postać dokumentów dla konkretnego zasobu danych. Modele te są definiowane ze schematów zdefiniowanych w plikach definiujących typy danych dla każdego z zasobów.

Dla przechowywania danych użytkownika jest stworzony interfejs IUserDocument oraz schemat modelu Users o nazwie UserSchema. Jest on definiowany za pomocą frameworka mongoose. Oprócz tego jest zdefiniowana klasa User, z której są czerpane metody do definicji schematu. Schemat definiuje następujące pola:

* name - pole typu String, jest wymagane czyli nie może go brakować w dokumentach w bazie danych. Jest używane do wskazania kto jest zalogowany (aplikacja angularowa),
* email - pole typu String, wymagane oraz musi być unikalne dla każdego dokumentu. E-mailem również logujemy się do aplikacji.
* role - pole typu String, domyślna wartość to "employee" (pracownik), 
* hash - pole typu String, jest wymagane. Przechowuje zaszyfrowane hasło.
* salt - pole typu String, jest wymagane do utworzenia hasła.

Dołączane do schematu metody to setPassword(), updatePassword() i validatePassword() do zarządzania i weryfikacji hasła, oraz generateJWT() do generowania tokenu JSON Web Token i toAuthJSON() do wysyłania danych użytkownika do aplikacji angularowej razem z wygenerowanym tokenem JSON Web Token.

Pomimo, że w pliku `fire-prevention-server/src/models/user.ts` jest definiowany model użytkownika w klasie Database z pliku `fire-prevention-server/src/models/database.ts` jest on inicjowany ponownie z obiektu połączenia. Jest to robione dlatego, że nawiązywane jest osobne połączenie i z obiektu tego połączenia (a dokładnie używając metody model() na rzecz obiektu połączenia), można utworzyć model, który będzie podpięty do bazy do odpowiedniej kolekcji. Mongoose zawsze podpina model o konkretnej nazwie do jego kolekcji posiadającej nazwę w liczbie mnogiej pisaną małymi literami. W ten sposób model User jest podpięty do kolekcji users.

Opisywane żądanie URL oprócz metody weryfikującej dane logowania (metoda signIn() z klasy AuthRouter) jest również obsługiwane przez inny middleware. W klasie Auth z pliku `fire-prevention-server/src/auth/auth.ts`, w konstruktorze klasy, definiowane są dwa middleware'y mające na celu zweryfikowanie tokenu JSON Web Token użytkownika i w obiekcie express. Request umieszcza zdekodowany obiekt user do dalszej weryfikacji. Middleware'y definiowane są za pomocą biblioteki express-jwt, będącej rozszerzeniem frameworka Express, udostępniającą funkcję jwt(). Nazwy definiowanych middleware'ów to required oraz optional. Jak ich nazwa wskazuje określają czy użytkownik jest wymagany do obsługi danego żądania przed przekazaniem sterowania do następnego middleware'a czy nie. W przypadku logowania użyty jest middleware optional. Jest on podpięty w klasie AuthRouter w metodzie init. Jest tam metoda definiująca obsługiwany url oraz middleware'y obsługujące ten url. Najpierw jest wpięty optional a potem signIn(). W przypadku logowania jest użyty optional ponieważ do samej obsługi logowania dane nie są wymagane. Dopiero przy innych żądaniach podpinany jest required, bo tam konkretne działania wymagają już zalogowania.

Zarówno middleware optional jak i required wykorzystują funkcję getTokenFromHeaders() zdefiniowaną w `fire-prevention-server/src/utils/utils.ts`. Pobiera ona token z nagłówków obiektu express.Request i zwraca go. Jeśli autoryzacja nie przeszła pomyślnie i tokena tam nie ma, to zwraca null co generuje odpowiedni błąd w middleware'rze tworzonym przez express-jwt.

### Zarządzanie użytkownikami

Ten moduł zawiera takie funkcjonalności jak zmiana hasła oraz stworzenie, usunięcie i edycję użytkowników. Z wyjątkiem zmiany hasła i wylistowania wszystkich użytkowników do reszty funkcjonalności wymagana jest rola admin. Jest to sprawdzane przez dodanie do obsługujących dany URL middleware'ów kolejnego middleware'a o nazwie adminRequired. Jest on wpinany po middleware'rze walidującym token JSON Web Token zwanym required z klasy Auth z pliku `fire-prevention-server/src/auth/auth.ts`. Na końcu serii middleware'ów, po ewentualnym adminRequired, wpinany jest właściwy middleware zdefiniowany w klasie dotyczącej route'owania adresów z danego modułu.

W przypadku zmiany hasła jest użyty inny middleware - adminOrItsOwn. Służy on do sprawdzenia, czy hasło jest zmieniane przez administratora (rola admin zalogowanego użytkownika) lub jeśli nie to czy użytkownik zmienia własne hasło, co jest dozwolone. Middleware adminOrItsOwn jest zdefiniowany w pliku `fire-prevention-server/src/middlewares/adminOrItsOwn.ts`.

Użytkownik jest samodzielnym zasobem w bazie danych. Oznacza to, że nie jest powiązany z innymi modelami. W przypadku MongoDB polega to na zagnieżdżeniu dokumentu zasobu, do którego odnosi się dokument wyższego poziomu. Dla przykładu: w omawianej aplikacji zadanie jest skorelowane z trzema innymi modelami w tym z użytkownikiem. Oznacza to, że dokument reprezentujący zadanie ma pole madeBy, którego wartością jest dokument przedstawiający użytkownika, który utworzył to zadanie.

#### Listowanie użytkowników

```
/api/user
```

Ten Url powoduje wyświetlenie użytkowników znajdujących się w bazie danych. Poza wymaganym zalogowaniem nie ma innych obostrzeń dotyczących dostępu do tego URLa.

URL jak zwykle jest wywoływany dwa razy. Najpierw metodą OPTIONS by poinformować o odpowiednich nagłówkach i metodzie żądania właściwego. Następnie następuje żądanie ustaloną metodą (GET) i odpowiedź serwera z zawartością z bazy danych.

Pierwsze wywołanie - metoda OPTIONS, wysyła same nagłówki bez innej zawartości:

    Access-Control-Request-Method: GET
    Origin: http://localhost:4200
    Referer: http://localhost:4200/
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    DNT: 1
    Access-Control-Request-Headers: authorization

Odpowiedź serwera - status 204 (No Content):

    Date: Mon, 13 May 2019 17:26:32 GMT
    X-Powered-By: Express
    Vary: Access-Control-Request-Headers
    Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE
    Access-Control-Allow-Origin: *
    Connection: keep-alive
    Access-Control-Allow-Headers: authorization
    Content-Length: 0

Drugie wywołanie - metoda GET:

    Accept: application/json, text/plain, */*
    Referer: http://localhost:4200/
    Origin: http://localhost:4200
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1jaC5qc29uQGdtYWlsLmNvbSIsIl9pZCI6IjVjZDFjZDkyOWVkZGM3YmQwNDAzMjg3NiIsInJvbGUiOiJlbXBsb3llZSIsImV4cCI6MTU1Nzg1NDczMSwiaWF0IjoxNTU3NzY4MzMxfQ.k8xIuEcI8k1D2GC1fVYwCP8CxYTzvav3OJqsLO3Rids
    DNT: 1

Odpowiedź serwera - status 200 (OK):

    Access-Control-Allow-Origin: *
    Date: Mon, 13 May 2019 17:26:32 GMT
    Connection: keep-alive
    X-Powered-By: Express
    ETag: W/"82-s+PiczUUK9hP+DBop+P6SlFnyxA"
    Content-Length: 130
    Content-Type: application/json; charset=utf-8

    {"ok":true,"data":[{"_id":"5cd1cd929eddc7bd04032876","name":"Artur","email":"mch.json@gmail.com","role":"employee"}],"message":""}

W obydwu wywołaniach tego URL widzimy najpierw poinformowanie a potem faktyczne przesłanie nagłówka Authorization. Jest to nagłówek dostarczający danych do autoryzacji użytkownika. Tutaj jest to przesłanie tokena JSON Web Token przy pomocy protokołów używanych przez OAuth 2.0 dlatego pierwsza wartość oznaczająca typ autentykacji to Bearer. Typ ten tyczy się właśnie przesyłania tokenów OAuth 2.0.

Odpowiedź serwera to JSON z danymi z bazy danych. Ponieważ jest to wylistowanie użytkowników, mamy tu tablicę data z dokumentami z bazy danych, ale pozbawionymi danych wrażliwych jak hash czy salt oraz dodatkowe dwa pola ok i message. W przypadku błędów ok zmienia swoją wartość na false, a message zawiera komunikat błędu.

Żądanie listujące użytkowników jest obsługiwane przez klasę UserRouter zdefiniowaną w pliku `fire-prevention-server/src/routes/user.ts` za pomocą metody getUsers(). Metoda getUsers() jest wpinana w metodzie init() jako ostatni middleware do obsługi tego URLa zaraz po middleware'rze required sprawdzającego przesyłany do serwera token w wyżej omawianym nagłówku Authorization. Metoda getUsers() używa klasy UserBackend z pliku `fire-prevention-server/src/backends/user-backend.ts` do pobrania danych z bazy danych. Klasa UserBackend to właściwie repozytorium zapytań bazodanowych dla zasobów związanych z danymi użytkownika. Każdy zasób danych ma zdefiniowany swój backend we wspomnianym katalogu.

W metodzie getUsers() z klasy UserRouter jest użyta metoda getAll() z klasy UserBackend. Metoda ta za pomocą omawianej już klasy Database z pliku `fire-prevention-server/src/models/database.ts` pobiera z bazy danych, z kolekcji users, wszystkich znajdujących się tam użytkowników. Następnie redefiniuje tablicę pobranych obiektów poprzez metodę map(), gdzie każdy element jest instancją klasy User z fire-prevention/src/models/user.ts i jest inicjowany elementem z tablicy pobranych użytkowników, po których iteruje metoda map. Po utworzeniu instancji i inicjalizacji danymi klasy User wywoływana jest metoda z tej klasy getNoSensitiveData(), która wyrzuca dane dotyczące hasła (pola hash i salt).

Po pobraniu i obrobieniu danych przez metodę getAll() z klasy UserBackend metoda getUsers() z klasy UserRouter wysyła dane za pomocą express.Response.json() przy dodatkowej pomocy klasy CustomResponse zdefiniowanej w pliku `fire-prevention-server/src/models/CustomResponse.ts`. Po utworzeniu instacji tej klasy i przekazaniu odpowiednich danych do konstruktora wywoływana jest jeszcze metoda z tej klasy get(), która zwraca obiekt z ustawionych w konstruktorze danych.

Konstruktor klasy CustomResponse przyjmuje następujące dane:

* ok - wartość typu boolean przyjmująca true w przypadku braku błędów lub false jeśli wystąpi błąd,
* data - najczęściej tablica obiektów pobranych z bazy danych jeśli ok wynosi true lub też pusta tablica jesli w bazie danych nie ma żadnych dokumentów dotyczących odpowiedniego zasobu. Jeśli ok wynosi false wtedy zamiast tablicy danych przekazywany jest obiekt błędu,
* message - wartość typu string, domyślnie pusta. W przypadku ok wynoszącego false może zawierać komunikat błędu. W przypadku ok wynoszącego true może zawierać komunikat o powodzeniu jakiejś operacji (na przykład utworzenie użytkownika).
* code - wartość typu number, domyślnie null. Nie używana.

#### Tworzenie nowego użytkownika

```
/api/user
```

Ten URL jest używany w celu utworzenia nowego użytkownika. Oprócz standardowego wymagania zalogowanego użytkownika ten URL jest dodatkowo jeszcze zastrzeżony dla użytkowników mających rolę administratora.

Wywoływany jest dwukrotnie. Najpierw metodą OPTIONS w celu ustawienia odpowiednich nagłówków i metody żądania właściwego (metoda POST i nagłówki Authorization z tokenem użytkownika oraz Content-Type z informacją o formacie JSON). Następnie żądanie właściwe z wymienionymi nagłówkami i metodą POST. Jak zawsze przy pierwszym wywołaniu ustawiany jest status 204 No Content.

Pierwsze żądanie - metoda OPTIONS, wysyła tylko nagłówki bez innej zawartości:

    Access-Control-Request-Method: POST
    Origin: http://localhost:4200
    Referer: http://localhost:4200/
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    DNT: 1
    Access-Control-Request-Headers: authorization,content-type

Odpowiedź serwera (status 204 No Content):

    Date: Sun, 19 May 2019 13:49:04 GMT
    X-Powered-By: Express
    Vary: Access-Control-Request-Headers
    Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE
    Access-Control-Allow-Origin: *
    Connection: keep-alive
    Access-Control-Allow-Headers: authorization,content-type
    Content-Length: 0

Drugie żądanie - metoda POST z danymi wypełnionymi przy tworzeniu użytkownika:

    Accept: application/json, text/plain, */*
    Referer: http://localhost:4200/
    Origin: http://localhost:4200
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1jaC5qc29uQGdtYWlsLmNvbSIsIl9pZCI6IjVjZDFjZDkyOWVkZGM3YmQwNDAzMjg3NiIsInJvbGUiOiJhZG1pbiIsImV4cCI6MTU1ODM1OTU5MSwiaWF0IjoxNTU4MjczMTkxfQ.wJqL7PNCIFNG09iMjU5EnPWEH484CWFFhfHmjloFEFY
    DNT: 1
    Content-Type: application/json

    {"user":{"name":"Shiram","email":"shiram@gets.fs","role":"employee","password":"jakiesdobrehaslo"}}

Odpowiedź serwera - status 200 (OK) oraz użytkownik już po utworzeniu w bazie danych, oczywiście bez danych wrażliwych:

    Access-Control-Allow-Origin: *
    Date: Sun, 19 May 2019 13:49:04 GMT
    Connection: keep-alive
    X-Powered-By: Express
    ETag: W/"96-89y8Wgp3S6Mfb2R8zvsD65LPFJs"
    Content-Length: 150
    Content-Type: application/json; charset=utf-8

    {"ok":true,"data":{"_id":"5ce15ed0a3e83114e88302fe","name":"Shiram","email":"shiram@gets.fs","role":"employee"},"message":"User created successfully"}

Ten URL jest obsługiwany w klasie UserRouter zdefiniowanej w pliku `fire-prevention-server/src/routes/user.ts`. W klasie tej do obsługi żądania podpięta jest metoda createUser(). Jest to oczywiście klasyczny middleware obsługujący każdy URL.

Wewnątrz createUser() najpierw sprawdzany jest fakt wysłania danych użytkownika, którego chcemy stworzyć. Realizuje to funkcja checkRequiredRequestData zdefiniowana w pliku `fire-prevention-server/src/models/CustomResponse.ts`. Przeszukuje ona obiekt typu express.Request na obecność obiektu user, który jest widoczny w drugim żądaniu omawianego URLa w części, którą wysyła przeglądarka do serwera. Funkcja checkRequiredRequestData() przeszukuje obiekt używając do tego funkcji get() z biblioteki lodash. Jest to biblioteka dostarczająca dużo pomocniczych funkcji do pracy na obiektach i tablicach.

Jeśli funkcja go nie znajdzie przekazuje informacje o błędzie do funkcji handleError() zdefiniowanej w tym samym pliku co checkRequiredRequestData(), która wysyła status 500 i komunikat o błędzie do przeglądarki. Informacja jest generowana za pomocą CustomResponse, który w tym przypadku dostaje wartość false dla parametru ok, ponieważ nastąpił błąd, pusty obiekt danych, ponieważ nie pobieraliśmy nic z bazy oraz komunikat błędu jako message.

Jeśli jednak obiekt user zostaje znaleziony, oznacza to, że dane zostały przesłane. W takim wypadku obiekt jest zapisywany do bazy danych poprzez klasę UserBackend zdefiniowaną w `fire-prevention-server/src/backends/user-backend.ts`. Klasa ta służy do manipulowania danymi użytkownika w bazie danych. Każdy zasób w systemie ma swój backend właśnie w katalogu `fire-prevention-server/src/backends`. Inaczej można takie klasy określić jako repozytoria zapytań bazodanowych.

W tej klasie metoda insertOne() inicjalizuje klasę User zdefiniowaną w `fire-prevention-server/src/models/user.ts`. Następnie ustawia hasło poprzez metodę setPassword w klasie User, a potem poprzez klasę Database zdefiniowaną w `fire-prevention-server/src/models/database.ts` i poprzez inicjalizowane w tej klasie modele dane są zapisywane do bazy danych. Odpowiedź z bazy danych jest używana do tego, aby zwrócić nowo instancję klasy User bez wrażliwych danych.

Gdy klasa UserBackend zapisze dane do bazy i zwróci je, metoda createUser() w klasie UserRouter wysyła je do przeglądarki. Dane oczywiście wysłane są w formacie JSON w postaci wygenerowanej przez CustomResponse z `fire-prevention-server/src/models/CustomResponse.ts`. Przykład odesłanych danych jest w drugim żądaniu w części dotyczącej odpowiedzi serwera. Tutaj już dane wyglądają znajomo: ok ma wartość true, ponieważ nie ma żadnych błędów, data zawiera obiekt z użytkownikiem zapisanym do bazy, dlatego pojawia się pole _id, a message zawiera komunikat o poprawności operacji tworzenia użytkownika.

URL ten oprócz omawianej metody createUser z klasy UserRouter i middleware'a required z klasy Auth, jest obsługiwany przez jeszcze jeden middleware. Chodzi mianowicie o funkcję adminRequired() zdefiniowaną w pliku `fire-prevention-server/src/middlewares/adminRequired.ts`. Jest ona wpięta między required a createUser(). Działa ona w ten sposób, że za pomocą wcześniej omawianej funkcji getTokenFromHeaders() pobiera token z nagłówków dostępnych w obiekcie typu express.Request. Jeśli token tam się znajduje, to jest on dekodowany do zwykłego obiektu z danymi użytkownika i sprawdzana jest rola zalogowanej osoby. Jeśli osoba ta ma rolę admin, czyli jest administratorem, obsługa URLa przechodzi do następnego middleware'a (funkcji createUser() z klasy UserRouter). Jeśli zalogowany użytkownik ma inną rolę wysyłana jest informacja o błędzie przy użyciu klasy CustomResponse jako reprezentacji błędu jaki jest przesyłany. Błąd jest wysyłany ze statusem 403 (forbidden).

#### Usuwanie użytkownika

```
/api/user
```

Ten URL jest używany do usuwania użytkownika. Tak samo jak utworzenie użytkownika, usuwanie wymaga praw administratora (roli admin) i dlatego ten URL również jest chroniony poprzez middleware adminRequired. Usuwanie użytkownika nie polega na wskazaniu zasobu za pomocą adresu URL wywołanego metodą DELETE, ale raczej przy jego wywołaniu wspomnianą metodą wysyłany jest wymyślony do tego celu nagłówek iditem, którego wartość to pole _id danego zasobu z bazy danych.

Wywoływany jest (jak każdy opisany do tej pory URL) dwukrotnie. Najpierw metodą OPTIONS w celu ustawienia nagłówków Authorization i właśnie iditem, oraz w celu ustawienia metody DELETE. Następnie już wywoływany wcześniej ustaloną metodą ze wspomnianymi nagłówkami.

Pierwsze wywołanie - metoda OPTIONS, ustawia nagłówki i metodę:

    Access-Control-Request-Method: DELETE
    Origin: http://localhost:4200
    Referer: http://localhost:4200/
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    DNT: 1
    Access-Control-Request-Headers: authorization,iditem

Odpowiedź serwera - status 204 (no Content):

    Date: Mon, 20 May 2019 18:54:03 GMT
    X-Powered-By: Express
    Vary: Access-Control-Request-Headers
    Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE
    Access-Control-Allow-Origin: *
    Connection: keep-alive
    Access-Control-Allow-Headers: authorization,iditem
    Content-Length: 0

Drugie wywołanie - metoda DELETE i odpowiednie opisane już nagłówki w tym iditem z odpowiednim id dokumentu w bazie danych:

    Accept: application/json, text/plain, */*
    Referer: http://localhost:4200/
    Origin: http://localhost:4200
    iditem: 5cec165fa3e83114e8830302
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1jaC5qc29uQGdtYWlsLmNvbSIsIl9pZCI6IjVjZDFjZDkyOWVkZGM3YmQwNDAzMjg3NiIsInJvbGUiOiJhZG1pbiIsImV4cCI6MTU1ODQ2MzQxOSwiaWF0IjoxNTU4Mzc3MDE5fQ.k5oRoGfCHHSshl02wPPyTuW1heSXWMQrTG0LxQVyBKM
    DNT: 1

Odpowiedź serwera - status operacji, usunięty zasób (użytkownik) i wiadomość o pomyślnym przebiegu operacji:

    Access-Control-Allow-Origin: *
    Date: Mon, 20 May 2019 18:54:03 GMT
    Connection: keep-alive
    X-Powered-By: Express
    ETag: W/"8f-JYA3Pp9mNSSYg0IYydFH+yzKgMQ"
    Content-Length: 143
    Content-Type: application/json; charset=utf-8

    {"ok":true,"data":{"_id":"5cec165fa3e83114e8830302","name":"User","email":"user@users.com","role":"employee"},"message":"User deleted successfully"}

URL obsługiwany jest w klasie UserRouter, gdzie obsługiwane są wszystkie URLe dotyczące użytkowników. Do obsługi procesu usuwania delegowana jest metoda deleteUser(), która jest wpinana jako ostatni middleware przy obsłudze tego adresu w metodzie init() klasy UserRouter.

Metoda deleteUser(), zgodnie z wcześniejszym opisem, za pomocą funkcji checkRequiredRequestData() sprawdza czy w obiekcie express.Request w nagłówkach znajduje się nagłówek iditem. Jeśli tak jest  w istocie to zawartość tego nagłówka jest przekazywana do metody deleteOne() z obiektu userBackend będący instancją klasy UserBackend z pliku `fire-prevention-server/src/backends/user-backend.ts`. Zaś metoda deleteOne() z klasy UserBackend poprzez klasę Database i przez dostępny przez statyczną właściwość models model użytkonwika zdefiniowany w mongoose wykonuje operację findByIdAndRemove. Znaleziony i natychmiastowo usunięty dokument jest zwracany przez metodę deleteOne().

Na koniec w metodzie deleteUser() w klasie UserRouter usunięty dokument jest zwracany poprzez sformowanie odpowiedniego obiektu odpowiedzi przez klasę CustomResponse. Chodzi o obiekt jaki jest pokazany przy odpowiedzi serwera na adres wywołany metodą delete. Oprócz statusu operacji (pola ok mającego wartość true) i komunikatu o powodzeniu operacji (pola message) tworzony jest obiekt data z usuniętym użytkownikiem, ale pozbawionym ważnych informacji (pól hash i salt do bezpiecznego przechowywania i szyfrowania hasła). Jest to osiągane poprzez inicjalizację obiektu kllasy User z `fire-prevention-server/src/models/user.ts` i narzecz obiektu klasy User wywoływana jest metoda getNoSensitiveData(). Na koniec na rzecz klasy CustomResponse wywoływana jest metoda get(), która zwraca JSONa z opisanymi informacjami.

#### Edycja Użytkownika

```
/api/user
```

Ten URL służy do edycji danych użytkownika (nazwy, e-maila oraz roli). URL jak wszystkie poprzednie operujące na użytkownikach jest zabezpieczony dodatkowo middleware'em adminRequired. Zmienione dane są wysyłane metodą PATCH w postaci JSONa, natomiast serwer odsyła również JSONa z poprzednimi danymi jakie były w bazie danych przy _id tego użytkownika. Wygląda to podobnie do tworzenia nowego użytkonwika, ale tutaj pole _id jest wysyłane również do serwera, w celu poinformowania, o którego użytkownika chodzi.

Standardowo URL jest wywoływany dwukrotnie. Pierwsze wywołanie to ustawienie nagłówków ponownie jak przy tworzeniu użytkonwika - Authorization oraz Content-Type. Drugie to właściwe wywołanie z odpowiednimi informacjami do zmiany na serwerze.

Pierwsze wywołanie - metoda OPTIONS, ustawienie nagłówków Authorization i Content-Type oraz metody PATCH:

    Access-Control-Request-Method: PATCH
    Origin: http://localhost:4200
    Referer: http://localhost:4200/
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    DNT: 1
    Access-Control-Request-Headers: authorization,content-type

Odpowiedź serwera - status 204 (No Content):

    Date: Tue, 28 May 2019 17:04:54 GMT
    X-Powered-By: Express
    Vary: Access-Control-Request-Headers
    Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE
    Access-Control-Allow-Origin: *
    Connection: keep-alive
    Access-Control-Allow-Headers: authorization,content-type
    Content-Length: 0

Drugie wywołanie - metoda PATCH z danymi do zmiany w bazie danych:

    Accept: application/json, text/plain, */*
    Referer: http://localhost:4200/
    Origin: http://localhost:4200
    User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
    Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1jaC5qc29uQGdtYWlsLmNvbSIsIl9pZCI6IjVjZDFjZDkyOWVkZGM3YmQwNDAzMjg3NiIsInJvbGUiOiJhZG1pbiIsImV4cCI6MTU1OTE0NjI3NiwiaWF0IjoxNTU5MDU5ODc2fQ.emlKnEa8WQF2CGyAGrViApxFwKN1GmPB-NGwMlFy9CU
    DNT: 1
    Content-Type: application/json

    {"user":{"name":"Rutra","_id":"5cd1cd929eddc7bd04032876","email":"rutra@ar.tr","role":"admin"}}

Odpowiedź serwera - status 200 (OK) po pomyślnej aktualizacji danych użytkownika:

    Access-Control-Allow-Origin: *
    Date: Tue, 28 May 2019 17:04:54 GMT
    Connection: keep-alive
    X-Powered-By: Express
    ETag: W/"96-EJWSUSzY1w77bu4UXlhg6dXpRic"
    Content-Length: 150
    Content-Type: application/json; charset=utf-8

    {"ok":true,"data":{"_id":"5cd1cd929eddc7bd04032876","name":"Artur","email":"mch.json@gmail.com","role":"admin"},"message":"User updated successfully"}

URL jest obsługiwany w klasie UserRouter. Tam do obsługi omawianego URLa dla metody PATCH podpięta została jako ostatni middleware metoda updateUser(). Sprawdza ona najpierw za pomocą funkcji checkRequiredRequestData czy w żądaniu (obiekt express.Request) są dane użytkownika. Jeśli tak, to za pomocą metody updateOne() z klasy UserBackend dokonuje aktualizacji danych.

Metoda updateOne() z klasy UserBackend dostaje obiekt klasy User z pliku `fire-prevention-server/src/models/user.ts`, oraz jako drugi parametr pole _id użytkownika. Jest to pole identyfikujące dokument w bazie MongoDB dodawane automatycznie przy tworzeniu dowolnego dokumentu w dowolnej kolekcji. Poprzez klasę Database i zainicjowany w niej model Users metoda updateOne() z UserBackend wyszukuje użytkownika po podanym jako drugi parametr polu _id i jednocześnie aktualizuje jego dane. Wywołanie metody getNoSensitiveData() w klasie User ma na celu nieprzekazanie danych wrażliwych jak hasło.

Po aktualizacji danych metoda updateOne() klasy UserBackend zwraca nową instancję klasy User inicjowaną zwróconymi danymi po zmianie danych użytkownika. Są to poprzednio zapisane dane jeszcze z przed aktualizacji, co widać w odpowiedzi serwera w drugim żądaniu omawianego URLa. Dane są zwracane oczywiście po wykluczeniu danych wrażliwych przez ponowne wywołanie getNoSensitiveData() na nowej instancji klasy User.

Po zwróceniu danych z metody updateOne() metoda obsługująca żądanie (updateUser()) zwraca dane do przeglądarki w postaci JSON przy pomocy obiektu CustomResponse z informacją o sukcesie operacji.

#### Zmiana hasła użytkownika

```
/api/user/password
```

> Uwaga! Napisać tą sekcję dokumentacji po poprawieniu błędu z wysyłką statusu 404 po poprawnej aktualizacji hasła użytkownika.
