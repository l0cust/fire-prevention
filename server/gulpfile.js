const gulp = require('gulp');
const ts = require('gulp-typescript');
const watch = require('gulp-watch');
const nodemon = require('gulp-nodemon');
const pump = require('pump');
const uglify =  require('gulp-uglify');
const tsProject = ts.createProject('tsconfig.json');


gulp.task('compile', () => {
  const tsResult = tsProject.src()
  .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('copy', () => {
  const tsResult = gulp.src('src/**/*.json')
  .pipe(gulp.dest('dist'));
});

gulp.task('watch', () => {
  watch('**/*.ts', { interval: 2000 }, gulp.series('compile'));
});

gulp.task('server', () => {
  nodemon({
      script: 'bin/www',
      watch: ["dist/"],
      ext: 'js'
  }).on('restart', () => {
      gulp.src('bin/www');
  });
});

gulp.task('livereload', gulp.parallel('compile', 'copy', 'watch', 'server'));

gulp.task('build', gulp.parallel('compile', 'copy'), (cb) => {
  pump(
    [
      gulp.src('dist/**/*.js'),
      uglify(),
      gulp.dest('dist')
    ],
      cb
    );
  }); 

gulp.task('default', gulp.series('livereload'));