import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import * as cors from 'cors';
import errorHandler = require('errorhandler'); // tslint:disable-line
import {Database} from './models/database';
import {ConfigPassport} from './auth/config-passport';

import UserRouter from './routes/user'; // tslint:disable-line
import AuthRouter from './routes/auth'; // tslint:disable-line
import CompanyRouter from './routes/company'; // tslint:disable-line
import FloorRouter from './routes/floor'; // tslint:disable-line
import CategoryTaskRouter from './routes/category-task'; // tslint:disable-line
import TaskRouter from './routes/task'; // tslint:disable-line
import ImageRouter from './routes/image'; // tslint:disable-line

import {CustomResponse} from './models';

/**
 * This is the main file of the server.
 * Connects with the database
 * Assign the routes with their controllers
 */
export class Server {
	public app: express.Application;
	database: any;
	configPassport: any;

	public static bootstrap(): Server {
		return new Server();
	}

	constructor() {
		this.app = express();
		this.database = new Database();
		this.configPassport = new ConfigPassport();
		this.middlewares();
		this.routes();
		this.errorHandlers();
	}

	/** Global error handler.
	 * If from any point is launched an error (next(err)) that will end here
	 */
	public errorHandlers() {
		// Send next middleware message as err content
		this.app.use((err, req, res, next) => {
			if (err.code) {
				console.error('Custom Error: ', err);
				res.status(err.code).send(err);
			} else {
				next(err);
			}
		});
	}

	public routes() {
		this.app.use('/api/user', UserRouter); // tslint:disable-line
		this.app.use('/api/auth', AuthRouter); // tslint:disable-line
		this.app.use('/api/company', CompanyRouter); // tslint:disable-line
		this.app.use('/api/floor', FloorRouter); // tslint:disable-line
		this.app.use('/api/categorytask', CategoryTaskRouter); // tslint:disable-line
		this.app.use('/api/task', TaskRouter); // tslint:disable-line
		this.app.use('/api/image', ImageRouter); // tslint:disable-line
	}

	public middlewares() {
		this.app.use(express.static(path.join(__dirname, 'public')));
		this.app.use(cors());
		this.app.use(logger('dev'));
		this.app.use(bodyParser.json());
		this.app.use(
			bodyParser.urlencoded({
				extended: true
			})
		);
		// catch 404 and forward to error handler
		this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
			err.status = 404;
			next(err);
		});
	}
}
