import { Document, Schema, Model, model } from 'mongoose';

export class CategoryTask {
  _id: any;
  name: string;
  color: string;
  hasType: boolean;
  hasMeasure: boolean;
  hasAmount: boolean;

  constructor(json: any) {
    if (json) {
      this.name = json.name;
      if (json._id) {
        this._id = json._id;
      }
      if (json.color !== null) {
        this.color = json.color;
      }
      this.hasType = json.hasType;
      this.hasMeasure = json.hasMeasure;
      this.hasAmount = json.hasAmount;
    }
  }
}

export let CategoryTaskSchema: Schema = new Schema({
  name: { type: String, required: true },
  color: { type: String },
  hasType: { type: Boolean },
  hasMeasure: { type: Boolean },
  hasAmount: { type: Boolean }
});

export interface ICategoryTaskDocument extends CategoryTask, Document {}

export const CategoryTasks: Model<ICategoryTaskDocument> = model<ICategoryTaskDocument>(
  'CategoryTask',
  CategoryTaskSchema
);
