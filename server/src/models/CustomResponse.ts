import { Router, Request, Response, NextFunction } from 'express';
import { get } from 'lodash';

export class CustomResponse {
  ok: boolean;
  data: any;
  message: string;
  code?: number;

  constructor(ok: boolean, data: any, message: string = '', code: number = null) {
    this.ok = ok;
    this.data = data;
    this.message = message;
    this.code = code;
  }

  get() {
    return { ok: this.ok, data: this.data, message: this.message };
  }
}

export function handleError(res: Response, error: any, message: string = ''): any {
  // Duplicate key error
  if (/E11000/i.test(error.message)) {
    message = 'Database error due to duplicate field. Please choose a different one: ';

    if (/email/i.test(error.message)) {
      message += 'Email';
    }
  }

  // Field required missed error
  if (/required/i.test(error.message)) {
    message = 'Error: Field required not found: ';

    if (/name/i.test(error.message)) {
      message += 'Name';
    }
    if (/email/i.test(error.message)) {
      message += 'Email';
    }
  }

  // Attempt to update an user and user not found
  if (/cast/i.test(error.message)) {
    message = `Error: Item to update not found: ${error.value}`;
  }

  console.error(`CUSTOM ERROR:`);
  console.log(error);
  return res.status(500).json(new CustomResponse(false, error, message || error.message).get());
}

/** Ensures that the reques contains the mandatory parameters for each route
 * @path the path for the parameter inside of the Response object
 */
export function checkRequiredRequestData(res: Response, obj: any, path: string) {
  if (!get(obj, path, false)) {
    handleError(res, {}, `Missed data request: ${path}`);
    return false;
  }
  return true;
}
