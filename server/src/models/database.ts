import { Config } from '../config';
import {Roles} from '../utils/CustomEnums';

import mongoose = require('mongoose');
import {
	IUserDocument,
	UserSchema,
	ICompanyDocument,
	CompanySchema,
	IFloorDocument,
	FloorSchema,
	ITaskDocument,
	TaskSchema,
	ICategoryTaskDocument,
	CategoryTaskSchema,
	User
} from '.';
import {Schema, Model} from 'mongoose';

interface IDatabaseModels {
	Users?: Model<IUserDocument>;
	Companies?: Model<ICompanyDocument>;
	Floors?: Model<IFloorDocument>;
	Tasks?: Model<ITaskDocument>;
	CategoryTasks?: Model<ICategoryTaskDocument>;
}

export class Database {
	connection: mongoose.Connection;
	CONNECTION_URL: string = `${Config.dbUrl}/${Config.dbName}`;

	options = {
		authSource: ((Config.isProduction) ? Config.dbName : 'admin'),
		auth: {
			user: Config.dbUser,
			password: Config.dbPass,
		},
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false,
		keepAlive: true
	};

	static models: IDatabaseModels = {};

	constructor() {
		this.connection = mongoose.createConnection(this.CONNECTION_URL, this.options);
		this.connection.on("connected", (err) => {
			console.log("Connected to database succesfully.");
			this.initDatabase();
		});
		this.connection.on("error", (err) => {
			console.log("Error in database connection! " + err);
		});
		this.connection.on("close", (err) => {
			console.log("Connection to database has closed.");
		});
		Database.models.Users = this.connection.model<IUserDocument>('User', UserSchema);
		Database.models.Companies = this.connection.model<ICompanyDocument>('Company', CompanySchema);
		Database.models.Floors = this.connection.model<IFloorDocument>('Floor', FloorSchema);
		Database.models.Tasks = this.connection.model<ITaskDocument>('Task', TaskSchema);
		Database.models.CategoryTasks = this.connection.model<ICategoryTaskDocument>('CategoryTask', CategoryTaskSchema);
	}

	/*  Initialize database if empty  */
	private async initDatabase() {
		let userData: IUserDocument = await Database.models.Users.findOne({"role": Roles.Admin});
		if (!userData)
		{
			console.log("Database is empty. Initializing...");
			try {
				let user: User = new User({"name": Config.defaultAdminName, "email": Config.defaultAdminMail, "role": Roles.Admin});
				user.setPassword(Config.defaultAdminPassword);
				await Database.models.Users.create(user);
				console.log("Done.");
			}
			catch (err) {
				console.log("Failed to initialize database! " + err.message);
			}
		}
	}
}
