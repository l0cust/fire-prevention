import { Config } from '../config';
import { Document, Schema, Model, model } from 'mongoose';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';

export class User {
  _id: any;
  name: string;
  email: string;
  password: string;
  salt: string;
  role: string;
  hash: string;

  constructor(json: any)  {
    if (json) {
      this.name = json.name;
      this.email = json.email;
      this.password = json.password;
      this.role = json.role;
      if (json._id) {
        this._id = json._id;
      }
    }
  }

  getNoSensitiveData() {
    return {
      _id: this._id,
      name: this.name,
      email: this.email,
      role: this.role
    };
  }

  setPassword(password: string): void {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.updatePassword(password);
  }

  updatePassword(password: string): void {
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  }

  validatePassword(password: string): boolean {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
  }

  generateJWT(): any {
    return jwt.sign({
      email: this.email,
      _id: this._id,
      role: this.role,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24) // Expires in 24h
	}, Config.secretJWT);
  }

  toAuthJSON(): any {
    return {
      _id: this._id,
      name: this.name,
      email: this.email,
      role: this.role,
      token: this.generateJWT()
    };
  }

}

export let UserSchema: Schema = new Schema({
  name: { type: String, required: true },
  email:  { type: String, unique: true, required: true },
  role: { type: String, default: 'employee' },
  hash: { type: String, required: true },
  salt: String
});

UserSchema.methods.setPassword = User.prototype.setPassword;
UserSchema.methods.updatePassword = User.prototype.updatePassword;
UserSchema.methods.validatePassword = User.prototype.validatePassword;
UserSchema.methods.generateJWT = User.prototype.generateJWT;
UserSchema.methods.toAuthJSON = User.prototype.toAuthJSON;

export interface IUserDocument extends User, Document { }

export const Users: Model<IUserDocument> = model<IUserDocument>('User', UserSchema);
