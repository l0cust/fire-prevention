import { Document, Schema, Model, model } from 'mongoose';

export class Company {
  _id: any;
  name: string;

  constructor(json: any) {
    if (json) {
      this.name = json.name;
      if (json._id) {
        this._id = json._id;
      }
    }
  }
}

export let CompanySchema: Schema = new Schema({
  name: { type: String, required: true }
});

export interface ICompanyDocument extends Company, Document { }

export const Users: Model<ICompanyDocument> = model<ICompanyDocument>('Company', CompanySchema);
