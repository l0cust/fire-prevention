import { Document, Schema, Model, model, Types } from 'mongoose';

export class Floor {
  _id: any;
  name: string;
  original: boolean;
  company: Types.ObjectId;

  constructor(json: any) {
    if (json) {
      this.name = json.name;
      if (json.original !== null) {
        this.original = json.original;
      }
      if (json.company) {
        this.company = json.company;
      }
      if (json._id) {
        this._id = json._id;
      }
    }
  }
}

export let FloorSchema: Schema = new Schema({
  name: { type: String, required: true },
  original: { type: Boolean, default: true },
  company: { type: Schema.Types.ObjectId, ref: 'Company' }
});

export interface IFloorDocument extends Floor, Document { }

export const Floors: Model<IFloorDocument> = model<IFloorDocument>('Floor', FloorSchema);
