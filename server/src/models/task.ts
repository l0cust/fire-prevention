import { Document, Schema, Model, model, Types } from 'mongoose';

export class Task {
  _id: any;
  name: string;
  description: string;
  madeBy: Types.ObjectId;
  location?: Types.ObjectId;
  categoryTask?: Types.ObjectId;
  positionMarker: any;
  type: string;
  measure: number;
  amount: number;

  constructor(json: any) {
    if (json) {
      this.name = json.name;
      this.description = json.description ? json.description : '';
      if (json.madeBy !== null) {
        this.madeBy = json.madeBy;
      }
      if (json._id) {
        this._id = json._id;
      }
      if (json.categoryTask !== null) {
        this.categoryTask = json.categoryTask;
      }
      if (json.location !== null) {
        this.location = json.location;
      }
      this.positionMarker = json.positionMarker;
      this.type = json.type;
      this.measure = json.measure;
      this.amount = json.amount;
    }
  }
}

export let TaskSchema: Schema = new Schema({
  name: { type: String },
  description: { type: String, required: false },
  photosBefore: { type: [String] },
  photosAfter: { type: [String] },
  madeBy: { type: Schema.Types.ObjectId, ref: 'User' },
  location: { type: Schema.Types.ObjectId, ref: 'Floor' },
  categoryTask: { type: Schema.Types.ObjectId, ref: 'CategoryTask' },
  positionMarker: { type: Object },
  type: { type: String },
  measure: { type: Number },
  amount: { type: Number }
},
{
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
});

export interface ITaskDocument extends Task, Document {}

export const Tasks: Model<ITaskDocument> = model<ITaskDocument>('Task', TaskSchema);
