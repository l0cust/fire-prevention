import {Config} from '../config';
import {Request, Response, NextFunction} from 'express';
import {getTokenFromHeaders, decodeJwt} from './../utils/utils';
import {Roles} from './../utils/CustomEnums';
import {CustomResponse} from './../models/CustomResponse';

/** Throws error if the token is not from an admin user */
export function adminOrItsOwn(req: Request, res: Response, next: NextFunction) {
	if (Config.isGodMode) {
		next();
	} else {
		const token: string = getTokenFromHeaders(req);
		if (!token) {
			next(new CustomResponse(false, null, 'No token provided', 401));
		}

		const decodedToken: any = decodeJwt(token);
		if (decodedToken.role === Roles.Admin) {
			next();
		}

		if (req && req.body && req.body.id === decodedToken._id) {
			next();
		} else {
			next(new CustomResponse(false, null, 'Required admin role or being editing your own password', 403));
		}
	}
}
