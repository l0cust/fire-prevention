import {Request, Response, NextFunction} from 'express';
import {getTokenFromHeaders, decodeJwt} from './../utils/utils';
import {Roles} from './../utils/CustomEnums';
import {CustomResponse} from './../models/CustomResponse';
import {Config} from '../config';

/** Throws error if the token is not from an admin user */
export function adminRequired(req: Request, res: Response, next: NextFunction) {
	if (Config.isGodMode) {
		next();
	} else {
		const token: string = getTokenFromHeaders(req);
		if (!token) {
			next(new CustomResponse(false, null, 'No token provided', 401));
		}

		const decodedToken: any = decodeJwt(token);
		if (decodedToken.role === Roles.Admin) {
			next();
		} else {
			next(new CustomResponse(false, null, 'Required admin role', 403));
		}
	}
}
