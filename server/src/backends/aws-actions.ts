const AWS = require('aws-sdk');
import { Config } from '../config';
import {Request, Response} from 'express';
const IncomingForm = require('formidable').IncomingForm;
const fs = require('fs');
import {get} from 'lodash';

/**
 * This class store the methods to interact with the Digital Ocean Storage, using the api of Amazon Web Services
 */
export class AwsActions {
	spacesEndpoint;
	s3;
	constructor() {
		this.spacesEndpoint = new AWS.Endpoint(Config.bucketEndpoint);
		this.s3 = new AWS.S3({
			endpoint: this.spacesEndpoint,
			accessKeyId: Config.bucketKeyId,
			secretAccessKey: Config.bucketKeySecret
		});
	}

	public uploadImage(nameBucket: string, req: Request, privacity: string = 'private'): Promise<any> {
		return new Promise((resolve, reject) => {
			const form = new IncomingForm();
			form.on('file', (field, file) => {
				fs.readFile(file.path, (err, data) => {
					if (err) {
						reject(err);
					}
					const params = {
						Body: data,
						Bucket: nameBucket,
						ContentEncoding: file.type,
						Key: `${field}/${file.name}.png`,
						ACL: privacity
					};

					this.s3.upload(params, (err, data) => {
						if (err) {
							reject(err);
						}
						fs.unlink(file.path, err => {
							if (err) {
								reject(err);
							}
						});
						resolve({ok: true});
					});
				});
			});
			form.on('end', () => {});
			form.parse(req);
		});
	}

	public async downloadImage(nameBucket: string, imageKey: string, res: Response): Promise<any> {
		const params = {
			Bucket: nameBucket,
			Key: imageKey
		};
		res.attachment(<string> imageKey);
		const fileStream = this.s3.getObject(params).createReadStream();
		fileStream.pipe(res);
	}

	public listImages(bucketName: string, path: string): Promise<any> {
		return new Promise((resolve, reject) => {
			const params = {
				Bucket: bucketName,
				Delimiter: '/',
				Prefix: path
			};
			this.s3.listObjectsV2(params, (err, data) => {
				if (err) {
					reject(err);
				}
				resolve({keys: get(data, 'Contents', []).map(images => images.Key)});
			});
		});
	}

	public listFolders(bucketName: string, path: string): Promise<any> {
		return new Promise((resolve, reject) => {
			const params = {
				Bucket: bucketName,
				Delimiter: '/',
				Prefix: path
			};
			this.s3.listObjectsV2(params, (err, data) => {
				if (err) {
					reject(err);
				}
				const arrFolderKeys = data.CommonPrefixes.reduce((acc, val) => [...acc, val.Prefix], []);
				resolve(arrFolderKeys);
			});
		});
	}

	public deleteImage(bucketName: string, imageKey: string): Promise<any> {
		return new Promise((resolve, reject) => {
			const params = {
				Bucket: bucketName,
				Key: imageKey
			};
			this.s3.deleteObject(params, (err, data) => {
				if (err) {
					reject(err);
				}
				resolve({ok: true});
			});
		});
	}

	public async copyImage(bucketName: string, sourcePath: string, destinityPath: string) {
		return new Promise((resolve, reject) => {
			const params: any = {
				Bucket: bucketName,
				CopySource: `${bucketName}/${sourcePath}`,
				Key: destinityPath
			};
			this.s3.copyObject(params, (err, data) => {
				if (err) {
					reject(err);
				}
				resolve({ok: true});
			});
		});
	}

	public async removeFolder(bucketName: string, folderPath): Promise<any> {
		// Get all the subfolders
		const arrSubFolders: string[] = await this.listFolders(bucketName, folderPath);

		// Get all the images in each subfolder
		const resEleFolders = await Promise.all(arrSubFolders.map(key => this.listImages(bucketName, key)));

		// put all the images keys in one array
		const imagesFolder: string[] = resEleFolders.reduce((acc, val) => {
			return val && val.keys ? [...acc, ...val.keys] : acc;
		}, []);

		// Remove all the images on the folder and subfolder
		const deletes: any[] = imagesFolder.map(key => this.deleteImage(bucketName, key));
		const resDeletes = await Promise.all(deletes);

		// If all the removals are successful return true
		const result = resDeletes.reduce((acc, val) => acc && val.ok, true);
		return {ok: result};
	}
}
