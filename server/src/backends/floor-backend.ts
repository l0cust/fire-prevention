import { Database } from './../models/database';
import { Floor, IFloorDocument } from './../models';

export class FloorBackend {
  constructor() {}

  public async getAll(): Promise<any[]> {
    return (await Database.models.Floors.find().populate('company')).map(
      (floor: IFloorDocument) => new Floor(floor.toObject())
    );
  }

  public async getByCompany(_id): Promise<any[]> {
    return (await Database.models.Floors.find({ company: { _id } }).populate('company')).map(
      (floor: IFloorDocument) => new Floor(floor.toObject())
    );
  }

  public async insertOne(floor: any): Promise<any> {
    const floorToInsert: Floor = new Floor(floor);
    const insertResponse = await Database.models.Floors.create(floorToInsert);
    return new Floor(insertResponse);
  }

  public async deleteOne(_id) {
    return await Database.models.Floors.findByIdAndRemove(_id);
  }

  public async updateOne(floor: Floor, _id) {
    const updateResponse = await Database.models.Floors.findByIdAndUpdate(_id, floor);
    return new Floor(updateResponse.toObject());
  }

  public async getOne(_id) {
    const respons = await Database.models.Floors.findById(_id).populate('company');
    return new Floor(respons.toObject());
  }
}
