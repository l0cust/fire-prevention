import { Database } from './../models/database';
import { Company, ICompanyDocument } from './../models';

export class CompanyBackend {

  constructor() {}

  public async getAll(): Promise<any[]> {
    return (await Database.models.Companies.find())
      .map((company: ICompanyDocument) => new Company(company.toObject()));
  }

  public async insertOne(company: any): Promise<any> {
    const companyToInsert: Company = new Company(company);
    const insertResponse = await Database.models.Companies.create(companyToInsert);
    return new Company(insertResponse);
  }

  public async deleteOne(_id) {
    return await Database.models.Companies.findByIdAndRemove(_id);
  }

  public async updateOne(company: Company, _id) {
    const updateResponse = await Database.models.Companies.findByIdAndUpdate(_id, company);
    return new Company(updateResponse.toObject());
  }

  public async getOne(_id) {
    return await Database.models.Companies.findById(_id);
  }
}
