import { Database } from './../models/database';
import { Task, ITaskDocument, User } from './../models';

export class TaskBackend {
  constructor() {}

  public async getAll(): Promise<any[]> {
    return (await Database.models.Tasks.find()
      .populate('location')
      .populate('categoryTask')
      .populate('madeBy')).map((task: ITaskDocument) => {
      const taskMap = task.toObject();
      taskMap.madeBy = new User(taskMap.madeBy).getNoSensitiveData();
      return taskMap;
    });
  }

  public async getByFloor(_id): Promise<any[]> {
    return (await Database.models.Tasks.find({ location: { _id } })
      .populate('location')
      .populate('categoryTask')
      .populate('madeBy')).map((task: ITaskDocument) => {
      const taskObj = task.toObject();
      taskObj.madeBy = new User(taskObj.madeBy).getNoSensitiveData();
      return taskObj;
    });
  }

  public async insertOne(task: any): Promise<any> {
    const taskToInsert: Task = new Task(task);
    const insertResponse = await Database.models.Tasks.create(taskToInsert);
    return new Task(insertResponse);
  }

  public async deleteOne(_id) {
    return await Database.models.Tasks.findByIdAndRemove(_id);
  }

  public async updateOne(task: any, _id) {
    const updateResponse = await Database.models.Tasks.findByIdAndUpdate(_id, task);
    return new Task(updateResponse.toObject());
  }

  public async getOne(_id) {
    const task: ITaskDocument = await Database.models.Tasks.findById(_id)
      .populate('location')
      .populate('categoryTask')
      .populate('madeBy');
    const taskObj = task.toObject();
    taskObj.madeBy = new User(taskObj.madeBy).getNoSensitiveData();
    return taskObj;
  }
}
