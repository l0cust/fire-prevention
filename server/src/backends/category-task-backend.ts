import { Database } from '../models/database';
import { CategoryTask, ICategoryTaskDocument } from '../models';

export class CategoryTaskBackend {
  constructor() {}

  public async getAll(): Promise<any[]> {
    return (await Database.models.CategoryTasks.find()).map(
      (categoryTask: ICategoryTaskDocument) => new CategoryTask(categoryTask.toObject())
    );
  }

  public async insertOne(categoryTask: any): Promise<any> {
    const categoryTaskToInsert: CategoryTask = new CategoryTask(categoryTask);
    const insertResponse = await Database.models.CategoryTasks.create(categoryTaskToInsert);
    return new CategoryTask(insertResponse);
  }

  public async deleteOne(_id) {
    return await Database.models.CategoryTasks.findByIdAndRemove(_id);
  }

  public async updateOne(categoryTask: CategoryTask, _id) {
    const updateResponse = await Database.models.CategoryTasks.findByIdAndUpdate(_id, categoryTask);
    return new CategoryTask(updateResponse.toObject());
  }

  public async getOne(_id) {
    return await Database.models.CategoryTasks.findById(_id).populate('company');
  }
}
