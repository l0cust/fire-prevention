import {Config} from '../config';
import * as fs from 'fs';
import {Readable} from 'stream';
import * as path from 'path';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


export interface IFileManager {
	init(options: any): void;
	uploadImage(sourceFile: string, location: string): Promise<boolean>;
	downloadImage(location: string): Promise<Readable>;
	listImages(path: string): Promise<string[]>;
	listFolders(path: string): Promise<string[]>;
	deleteImage(location: string): Promise<boolean>;
	copyImage(sourcePath: string, destinationPath: string): Promise<boolean>;
	removeFolder(folderPath: string): Promise<boolean>;
}

export class LocalFileManager implements IFileManager {
	private serverRootPath: string;
	private folder: string;

	constructor() {
		this.serverRootPath = path.dirname(path.normalize(__dirname)) + '/';
		this.folder = this.serverRootPath + "images/";
	}
	init(options: any): void {
		if (options) {
			if (options.root) {
				let folder: string = options.root;
				if (path.isAbsolute(folder))
					this.folder = folder;
				else
					this.folder = this.serverRootPath + folder;
				if (!Config.isProduction)
					console.log("New file manager path is " + this.folder);
			}
		}
	}

	public uploadImage(sourceFile: string, location: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			var filePath = this.folder + location;
			fs.mkdir(path.dirname(filePath), {recursive: true}, (err) => {
				if (err) {
					if (!Config.isProduction)
						console.log("Cannot create directory " + path.dirname(filePath));
					reject(err);
				}
				fs.copyFile(sourceFile, this.folder + location, (err) => {
					if (err) {
						if (!Config.isProduction)
							console.log("Cannot copy " + sourceFile + " to " + this.folder + location);
						reject(err);
					} else
						resolve(true);
				});
			});
		});
	}

	public downloadImage(location: string): Promise<Readable> {
		return new Promise((resolve, reject) => {
			fs.stat(this.folder + location, (err, stats) => {
				if (err)
					reject(err)
				else
					if (stats.isFile())
						resolve(fs.createReadStream(this.folder + location));
					else
						reject(new Error("File does not exist"));
			})
		});
	}

	public listImages(path: string): Promise<string[]> {
		return new Promise((resolve, reject) => {
			fs.readdir(this.folder + path, "utf8", (err, filenames) => {
				if (err)
					reject(err);
				else {
					let files: string[] = [];
					let file: string;
					let stats: fs.Stats;
					let folder = this.folder + path;
					if ((folder.charAt(folder.length - 1) != '/') && (folder.charAt(folder.length - 1) != '\\'))
						folder += '/';
					for (let i = 0; i < filenames.length; i++) {
						file = filenames[i];
						if (file.charAt(0) != '.') {
							stats = fs.statSync(folder + file);
							if (stats.isFile()) {
								files.push(file);
							}
						}
					}
					resolve(files);
				}
			});
		});
	}

	public listFolders(path: string): Promise<string[]> {
		return new Promise((resolve, reject) => {
			fs.readdir(this.folder + path, "utf8", (err, filenames) => {
				if (err)
					reject(new Error(err.message));
				else {
					let files: string[];
					filenames.forEach((file) => {
						if (file.charAt(0) != '.') {
							fs.stat(file, (err, stat) => {
								if (!err) {
									if (stat.isDirectory())
										files.push(file);
								}
							});
						}
					});
					resolve(files);
				}
			});
		});
	}

	public deleteImage(location: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			fs.unlink(this.folder + location, (err) => {
				if (err)
					reject(err);
				else
					resolve(true);
			});
		});
	}

	public copyImage(sourcePath: string, destinationPath: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			fs.copyFile(this.folder + sourcePath, this.folder + destinationPath, (err) => {
				if (err)
					reject(err);
				else
					resolve(true);
			});
		});
	}

	public async removeFolder(folderPath: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			fs.readdir(this.folder + folderPath, "utf8", (err, filenames) => {
				if (err) reject(err);
				if (filenames && filenames.length > 0) {
					filenames.forEach((file) => {
						let name: string = this.folder + folderPath + file;
						fs.stat(name, (err, stat) => {
							if (err) reject(err);
							if (stat.isFile()) {
								fs.unlink(name, (err) => {});
							}
						});
					});
					resolve(true);
				} else {
					resolve(false);
				}
			});
		});
	}
}
