import { Database } from './../models/database';
import { User, IUserDocument } from './../models';

export class UserBackend {

  constructor() {}

  public async getAll(): Promise<any[]> {
    return (await Database.models.Users.find())
      .map((user: IUserDocument) => new User(user.toObject()).getNoSensitiveData());
  }

  public async insertOne(user: any): Promise<any> {
    const userToInsert: User = new User(user);
    userToInsert.setPassword(user.password);
    const insertResponse = await Database.models.Users.create(userToInsert);
    return new User(insertResponse).getNoSensitiveData();
  }

  public async deleteOne(_id) {
    return await Database.models.Users.findByIdAndRemove(_id);
  }

  public async updateOne(user: User, _id) {
    const updateResponse = await Database.models.Users.findByIdAndUpdate(_id, user.getNoSensitiveData());
    return new User(updateResponse.toObject()).getNoSensitiveData();
  }

  public async getOne(_id) {
    return await Database.models.Users.findById(_id);
  }

  public async updatePassword(user: User, password: string) {
    user.updatePassword(password);
    const updateResponse = await Database.models.Users.findByIdAndUpdate(user._id, { hash: user.hash });
    return new User(updateResponse.toObject()).getNoSensitiveData();
  }

}
