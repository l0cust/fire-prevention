import {Config} from '../config';
import * as jwt from 'express-jwt';
import {getTokenFromHeaders} from './../utils/utils';

interface IAuthObj {
	secret: string;
	userProperty: string;
	getToken: Function;
	credentialsRequired?: boolean;
}
export class Auth {
	required: any;
	optional: any;

	constructor() {
		if (Config.isGodMode) {
			this.required = jwt({
				secret: Config.secretJWT,
				userProperty: 'payload',
				getToken: getTokenFromHeaders,
				credentialsRequired: false
			});
		} else {
			this.required = jwt({
				secret: Config.secretJWT,
				userProperty: 'payload',
				getToken: getTokenFromHeaders
			});
		}
		this.optional = jwt({
			secret: Config.secretJWT,
			userProperty: 'payload',
			getToken: getTokenFromHeaders,
			credentialsRequired: false
		});
	}
}
