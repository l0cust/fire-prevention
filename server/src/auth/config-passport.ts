import { Database } from '../models/database';
import * as passport from 'passport';
import * as LocalStrategy from 'passport-local';

export class ConfigPassport {

  constructor() {
    this.init();
  }

  // Validation for the login
  init() {
    passport.use(new LocalStrategy({
      usernameField: 'user[email]',
      passwordField: 'user[password]'
    }, async (email, password, done) => {
      try {
        const user = await Database.models.Users.findOne({ email });
        if (!user || !user.validatePassword(password)) {
          return done(null, false, { error: 'email or password is invalid' });
        }
        return done(null, user);
      } catch (err) {
        return done(err);
      }
      
    }));
  }
}
