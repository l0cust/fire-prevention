import { Config } from '../config';
import { Request } from 'express';

export function getNameApp(): string {
  const nameApp: string = Config.appName;
  return nameApp;
}

export enum TypeObjects {
  User = 'User',
  Company = 'Company',
  Floor = 'Floor',
  CategoryTask = 'CategoryTask',
  Task = 'Task'
}

export function getFolderPath(type: string, iditem: string) {
  return `${type}/${iditem}/`;
}

export function getTokenFromHeaders(req: Request) {
  const {
    headers: { authorization }
  } = req;
  if (authorization && authorization.split(' ')[0] === 'Bearer') {
    return authorization.split(' ')[1];
  }
  return null;
}

/**
 * @param token token as string
 * @return decoded token as JSON object
 */
export function decodeJwt(token: string) {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  return JSON.parse(atobUTF8(base64));
}

/** Custom window.atob function implementation because atob() not available in node as default */
export function atobUTF8(b64Encoded) {
  return Buffer.from(b64Encoded, 'base64').toString('utf8');
}
