import * as fs from 'fs';

export class Config {
	static configPath: string = "config/config.json";

	static appName: string = "Fire Prevention Server";
	static isProduction: boolean = false;
	static isGodMode: boolean = false;
	static dbUrl: string = "mongodb://localhost:27017";
	static dbName: string = "fire";
	static dbUser: string = "root";
	static dbPass: string = "root";
	static secretJWT: string = "";
	static fileManager: string = "local";			//  "local" or "aws"
	static fileManagerRoot: string = "images/";		//  Remember about '/' at the end
	static bucketName: string = "digitalsolutions-sei";
	static bucketEndpoint: string = "ams3.digitaloceanspaces.com";
	static bucketKeyId: string = "id";
	static bucketKeySecret: string = "secret";
	static defaultAdminName: string = "admin";
	static defaultAdminMail: string = "admin";
	static defaultAdminPassword: string = "admin";

	public static init() {
		console.log('Loading config from ' + this.configPath + '...');
		Config.loadFromFile();
		Config.loadFromEnv();

		if (this.isProduction)
			this.isGodMode = false;

		if (this.isProduction)
			console.log('--------------------- STARTING PRODUCTION SERVER ---------------------'); // tslint:disable-line
		else
			console.log('--------------------- STARTING DEVELOPMENT SERVER ---------------------'); // tslint:disable-line
	};

	private static loadFromFile(): boolean {
		if (this.configPath) {
			try {
				var content: string = fs.readFileSync(__dirname + '/' + this.configPath, content);
				if (content) {
					var obj = JSON.parse(content);
					if (obj.appName)
						this.appName = obj.appName;
					if (obj.isProduction)
						this.isProduction = obj.isProduction;
					if (obj.isGodMode)
						this.isGodMode = obj.isGodMode;
					if (obj.dbUrl)
						this.dbUrl = obj.dbUrl;
					if (obj.dbName)
						this.dbName = obj.dbName;
					if (obj.dbUser)
						this.dbUser = obj.dbUser;
					if (obj.dbPass)
						this.dbPass = obj.dbPass;
					if (obj.secretJWT)
						this.secretJWT = obj.secretJWT;
					if (obj.fileManager)
						this.fileManager = obj.fileManager;
					if (obj.fileManagerRoot)
						this.fileManagerRoot = obj.fileManagerRoot;
					if (obj.bucketName)
						this.bucketName = obj.bucketName;
					if (obj.bucketEndpoint)
						this.bucketEndpoint = obj.bucketEndpoint;
					if (obj.bucketKeyId)
						this.bucketKeyId = obj.bucketKeyId;
					if (obj.bucketKeySecret)
						this.bucketKeySecret = obj.bucketKeySecret;
					if (obj.defaultAdminName)
						this.defaultAdminName = obj.defaultAdminName;
					if (obj.defaultAdminMail)
						this.defaultAdminMail = obj.defaultAdminMail;
					if (obj.defaultAdminPassword)
						this.defaultAdminPassword = obj.defaultAdminPassword;

					if (this.isProduction)
						this.appName += "-dev";
				}
			}
			catch (err) {
				console.log("Warning! There is no config file! " + err.message);
				return false;
			}

		} else
			return false;
		return true;
	}

	private static loadFromEnv(): boolean {
		if (process.env.db_name)
			this.appName = process.env.db_name;
		if (process.env.isProd)
			this.isProduction = (process.env.isProd === 'true') ? true : false;
		if (process.env.isGodMode)
			this.isGodMode = (process.env.isGodMode === 'true') ? true : false;
		if (process.env.database_url)
			this.dbUrl = process.env.database_url;
		if (process.env.db_name)
			this.dbName = process.env.db_name;
		if (process.env.db_user)
			this.dbUser = process.env.db_user;
		if (process.env.db_pass)
			this.dbPass = process.env.db_pass;
		if (process.env.secret_jwt)
			this.secretJWT = process.env.secret_jwt;
		if (process.env.bucket_name)
			this.bucketName = process.env.bucket_name;
		if (process.env.bucket_endpoint)
			this.bucketEndpoint = process.env.bucket_endpoint;
		if (process.env.bucket_key_id)
			this.bucketKeyId = process.env.bucket_key_id;
		if (process.env.bucket_key_secret)
			this.bucketKeySecret = process.env.bucket_key_secret;
		if (process.env.file_manager)
			this.fileManager = process.env.file_manager;
		if (process.env.file_manager_root)
			this.fileManagerRoot = process.env.file_manager_root;

		return true;
	}
}
