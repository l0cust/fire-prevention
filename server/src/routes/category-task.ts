import { Router, Request, Response, NextFunction } from 'express';
import { CategoryTask, CustomResponse, handleError, checkRequiredRequestData } from '../models';
import { CategoryTaskBackend } from '../backends';
import { Auth } from '../auth/auth';

export class CategoryTaskRouter {
  router: Router;
  auth: Auth;
  categoryTaskBackend: CategoryTaskBackend;

  constructor() {
    this.router = Router();
    this.auth = new Auth();
    this.categoryTaskBackend = new CategoryTaskBackend();
    this.init();
  }

  public async getCategoryTasks(req: Request, res: Response, next: NextFunction) {
    const resultQuery = await this.categoryTaskBackend.getAll();
    res.json(new CustomResponse(true, resultQuery).get());
  }

  public async createCategoryTask(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.categorytask')) return;
    const {
      body: { categorytask }
    } = req;
    try {
      const categoryTaskInserted = await this.categoryTaskBackend.insertOne(categorytask);
      const fullCategoryTaskInserted = await this.categoryTaskBackend.getOne(categoryTaskInserted._id);

      res.json(new CustomResponse(true, fullCategoryTaskInserted, 'CategoryTask created successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  public async deleteCategoryTask(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
    const {
      headers: { iditem }
    } = req;

    try {
      const deleteResponse = await this.categoryTaskBackend.deleteOne(iditem);
      res.json(new CustomResponse(true, new CategoryTask(deleteResponse), 'CategoryTask deleted successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  public async updateCategoryTask(req: Request, res: Response, next: NextFunction) {
    if (
      !checkRequiredRequestData(res, req, 'body.categorytask') ||
      !checkRequiredRequestData(res, req, 'body.categorytask._id')
    ) {
      return;
    }
    const {
      body: { categorytask }
    } = req;
    const _id = categorytask._id;

    try {
      const categoryTaskUpdated = await this.categoryTaskBackend.updateOne(new CategoryTask(categorytask), _id);
      const fullCategoryTaskUpdate = await this.categoryTaskBackend.getOne(categoryTaskUpdated._id);
      res.json(new CustomResponse(true, fullCategoryTaskUpdate, 'CategoryTask updated successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  init() {
    this.router.get('/', this.auth.required, this.getCategoryTasks.bind(this));
    this.router.post('/', this.auth.required, this.createCategoryTask.bind(this));
    this.router.delete('/', this.auth.required, this.deleteCategoryTask.bind(this));
    this.router.patch('/', this.auth.required, this.updateCategoryTask.bind(this));
  }
}

const categoryTaskRoutes = new CategoryTaskRouter();
categoryTaskRoutes.init();

export default categoryTaskRoutes.router;
