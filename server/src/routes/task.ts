import {Config} from '../config';
import {Router, Request, Response, NextFunction} from 'express';
import {Task, CustomResponse, handleError, checkRequiredRequestData} from './../models';
import {TaskBackend, AwsActions} from './../backends';
import {TypeObjects, getFolderPath} from './../utils/utils';
import {Auth} from './../auth/auth';
import {IFileManager, LocalFileManager} from './../backends/file-manager';

export class TaskRouter {
	router: Router;
	auth: Auth;
	taskBackend: TaskBackend;
	awsActions: AwsActions;
	manager: IFileManager;

	constructor() {
		this.router = Router();
		this.auth = new Auth();
		this.taskBackend = new TaskBackend();
		if (Config.fileManager == "local") {
			this.manager = new LocalFileManager();
			this.manager.init({"root": Config.fileManagerRoot});
		}
		if (Config.fileManager == "aws") {
			this.awsActions = new AwsActions();
		}
		this.init();
	}

	public async getTasks(req: Request, res: Response, next: NextFunction) {
		const resultQuery = await this.taskBackend.getAll();
		res.json(new CustomResponse(true, resultQuery).get());
	}

	public async getTask(req: Request, res: Response, next: NextFunction) {
		if (!checkRequiredRequestData(res, req, 'headers.taskid')) return;
		const {
			headers: {taskid}
		} = req;

		const resultQuery = await this.taskBackend.getOne(taskid);
		res.json(new CustomResponse(true, resultQuery).get());
	}

	public async getTasksByFloor(req: Request, res: Response, next: NextFunction) {
		if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
		const {
			headers: {iditem}
		} = req;
		const resultQuery = await this.taskBackend.getByFloor(iditem);
		res.json(new CustomResponse(true, resultQuery).get());
	}

	public async createTask(req: Request, res: Response, next: NextFunction) {
		if (!checkRequiredRequestData(res, req, 'body.task')) return;
		const {
			body: {task}
		} = req;

		if (task.madeBy && Object.keys(task.madeBy).length === 0) {
			delete task.madeBy;
		}
		if (task.location && Object.keys(task.location).length === 0) {
			delete task.location;
		}

		try {
			const taskInserted = await this.taskBackend.insertOne(task);
			const fullTaskInserted = await this.taskBackend.getOne(taskInserted._id);

			res.json(new CustomResponse(true, fullTaskInserted, 'Task created successfully').get());
		} catch (err) {
			handleError(res, err);
		}
	}

	/** Remove the images stored in the Cloud and if success, remove the task from the database */
	public async deleteTask(req: Request, res: Response, next: NextFunction) {
		if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
		const {
			headers: {iditem}
		} = req;

		try {
			const folderPath: string = getFolderPath(TypeObjects.Task, <string> iditem);
			const resultRemoveImagesBefore = await this.manager.removeFolder(folderPath + 'before/');
			const resultRemoveImagesAfter = await this.manager.removeFolder(folderPath + 'after/');
			if (resultRemoveImagesBefore || resultRemoveImagesAfter) {
				const deleteResponse = await this.taskBackend.deleteOne(iditem);
				res.json(new CustomResponse(true, new Task(deleteResponse), 'Task deleted successfully').get());
			} else {
				throw Error('Error removing the images from the cloud');
			}
		} catch (err) {
			if (err.code === 'ENOENT') {
				const deleteResponse = await this.taskBackend.deleteOne(iditem);
				res.json(new CustomResponse(true, new Task(deleteResponse), 'Task deleted successfully').get());
			} else {
				handleError(res, err);
			}
		}
	}

	public async updateTask(req: Request, res: Response, next: NextFunction) {
		if (!checkRequiredRequestData(res, req, 'body.task') || !checkRequiredRequestData(res, req, 'body.task._id')) {
			return;
		}
		const {
			body: {task}
		} = req;
		const _id = task._id;

		if (task.madeBy && Object.keys(task.madeBy).length === 0) {
			delete task.madeBy;
		}
		if (task.location && Object.keys(task.location).length === 0) {
			delete task.location;
		}

		try {
			const taskUpdated = await this.taskBackend.updateOne(new Task(task), _id);
			const fullTaskUpdate = await this.taskBackend.getOne(taskUpdated._id);
			res.json(new CustomResponse(true, fullTaskUpdate, 'Task updated successfully').get());
		} catch (err) {
			handleError(res, err);
		}
	}

	init() {
		this.router.get('/floor', this.auth.required, this.getTasksByFloor.bind(this));
		this.router.get('/taskid', this.auth.required, this.getTask.bind(this));
		this.router.get('/', this.auth.required, this.getTasks.bind(this));
		this.router.post('/', this.auth.required, this.createTask.bind(this));
		this.router.delete('/', this.auth.required, this.deleteTask.bind(this));
		this.router.patch('/', this.auth.required, this.updateTask.bind(this));
	}
}

const taskRoutes = new TaskRouter();
taskRoutes.init();

export default taskRoutes.router;
