import { Router, Request, Response, NextFunction } from 'express';
import { User, CustomResponse, handleError, checkRequiredRequestData } from './../models';
import { Auth } from './../auth/auth';
import { UserBackend } from './../backends';
import { adminRequired, adminOrItsOwn } from './../middlewares';

export class UserRouter {
  router: Router;
  auth: Auth;
  userBackend: UserBackend;

  constructor() {
    this.router = Router();
    this.auth = new Auth();
    this.userBackend = new UserBackend();
    this.init();
  }

  public async getUsers(req: Request, res: Response, next: NextFunction) {
    const resultQuery = await this.userBackend.getAll();
    res.json(new CustomResponse(true, resultQuery).get());
  }

  public async createUser(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.user')) return;
    const {
      body: { user }
    } = req;
    try {
      const userInserted = await this.userBackend.insertOne(user);
      res.json(new CustomResponse(true, userInserted, 'User created successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  public async deleteUser(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
    const {
      headers: { iditem }
    } = req;

    try {
      const deleteResponse = await this.userBackend.deleteOne(iditem);
      res.json(
        new CustomResponse(true, new User(deleteResponse).getNoSensitiveData(), 'User deleted successfully').get()
      );
    } catch (err) {
      handleError(res, err);
    }
  }

  public async updateUser(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.user') || !checkRequiredRequestData(res, req, 'body.user._id')) {
      return;
    }
    const {
      body: { user }
    } = req;
    const _id = user._id;

    try {
      const userUpdated = await this.userBackend.updateOne(new User(user), _id);
      res.json(new CustomResponse(true, userUpdated, 'User updated successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  public async updatePassword(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.password') || !checkRequiredRequestData(res, req, 'body.id')) return;
    const {
      body: { id, password }
    } = req;

    let userToUpdate: User;
    try {
      userToUpdate = await this.userBackend.getOne(id);
    } catch (err) {
      res.status(404).json(new CustomResponse(false, err, 'User to update password not found').get());
    }

    try {
      const updateResponse = await this.userBackend.updatePassword(userToUpdate, password);
      res.json(new CustomResponse(true, updateResponse, 'Password updated successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  init() {
    this.router.get('/', this.auth.required, this.getUsers.bind(this));
    this.router.post('/', this.auth.required, adminRequired, this.createUser.bind(this));
    this.router.delete('/', this.auth.required, adminRequired, this.deleteUser.bind(this));
    this.router.patch('/password', this.auth.required, adminOrItsOwn, this.updatePassword.bind(this));
    this.router.patch('/', this.auth.required, adminRequired, this.updateUser.bind(this));
  }
}

const userRoutes = new UserRouter();
userRoutes.init();

export default userRoutes.router;
