import { Router, Request, Response, NextFunction } from 'express';
import { Company, CustomResponse, handleError, checkRequiredRequestData } from './../models';
import { CompanyBackend } from './../backends';
import { Auth } from './../auth/auth';

export class CompanyRouter {
  router: Router;
  auth: Auth;
  companyBackend: CompanyBackend;

  constructor() {
    this.router = Router();
    this.auth = new Auth();
    this.companyBackend = new CompanyBackend();
    this.init();
  }

  public async getCompanies(req: Request, res: Response, next: NextFunction) {
    const resultQuery = await this.companyBackend.getAll();
    res.json(new CustomResponse(true, resultQuery).get());
  }

  public async createCompany(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.company')) return;
    const { body: { company } } = req;
    try {
      const companyInserted = await this.companyBackend.insertOne(company);
      res.json(new CustomResponse(true, companyInserted, 'Company created successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  public async deleteCompany(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
    const { headers: { iditem } } = req;

    try {
      const deleteResponse = await this.companyBackend.deleteOne(iditem);
      res.json(
        new CustomResponse(true, new Company(deleteResponse), 'Company deleted successfully').get()
      );
    } catch (err) {
      handleError(res, err);
    }
  }

  public async updateCompany(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.company') ||
        !checkRequiredRequestData(res, req, 'body.company._id')) return;
    const { body: { company } } = req;
    const _id = company._id;

    try {
      const companyUpdated = await this.companyBackend.updateOne(new Company(company), _id);
      res.json(new CustomResponse(true, companyUpdated, 'Company updated successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  init() {
    this.router.get('/', this.auth.required, this.getCompanies.bind(this));
    this.router.post('/', this.auth.required, this.createCompany.bind(this));
    this.router.delete('/', this.auth.required, this.deleteCompany.bind(this));
    this.router.patch('/', this.auth.required, this.updateCompany.bind(this));
  }
}

const companyRoutes = new CompanyRouter();
companyRoutes.init();

export default companyRoutes.router;
