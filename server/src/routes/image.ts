import {Config} from '../config';
import * as path from 'path';
import {Readable} from 'stream';
import {Router, Request, Response, NextFunction} from 'express';
import {Auth} from './../auth/auth';
import {checkRequiredRequestData, handleError} from './../models';
import {AwsActions, TaskBackend} from './../backends';
import {getNameApp, TypeObjects, getFolderPath} from './../utils/utils';
import * as uuidv1 from 'uuid/v1';
import {ObjectId} from 'bson';
import {IFileManager, LocalFileManager} from './../backends/file-manager';
const IncomingForm = require('formidable').IncomingForm;

const AWS = require('aws-sdk');

export class ImageRouter {
	manager: IFileManager;
	router: Router;
	auth: Auth;
	taskBackend: TaskBackend;

	spacesEndpoint = new AWS.Endpoint(Config.bucketEndpoint);
	s3 = new AWS.S3({
		endpoint: this.spacesEndpoint,
		accessKeyId: Config.bucketKeyId,
		secretAccessKey: Config.bucketKeySecret
	});

	awsActions: AwsActions;

	constructor() {
		this.router = Router();
		this.auth = new Auth();
		console.log("File manager is " + Config.fileManager);
		if (Config.fileManager == "local") {
			if (!Config.isProduction)
				console.log("Using local file manager.");
			this.manager = new LocalFileManager();
			this.manager.init({"root": Config.fileManagerRoot});
		}
		if (Config.fileManager == "aws") {
			if (!Config.isProduction)
				console.log("Using AWS file manager.");
			// this.manager = new LocalFileManager({"root": Config.fileManagerRoot});
			// this.manager.init({"bucket": Config.bucketName, "endpoint": Config.bucketEndpoint, "id": Config.bucketKeyId, "secret": Config.bucketKeySecret});
			this.awsActions = new AwsActions();
		}
		this.init();
		this.taskBackend = new TaskBackend();
	}

	public async uploadImage(req: Request, res: Response, next: NextFunction) {
		try {
			const form = new IncomingForm();
			form.on('file', (field, file) => {
				this.manager.uploadImage(file.path, field + "/" + file.name)
					.then((ret) => {
						if (ret)
							res.json({ok: true});
						else
							res.json({error: true});
					})
					.catch((err) => {
						handleError(res, err);
					});
			});
			form.on('end', () => {});
			form.parse(req);
		} catch (err) {
			handleError(res, err);
		}
	}

	public async downloadImage(req: Request, res: Response, next: NextFunction) {
		try {
			let path = req.params[0];
			let file = req.params[1];
			this.manager.downloadImage(path + "/" + file).
				then((ret) => {
					res.attachment(<string> file);
					ret.pipe(res);
				})
				.catch((err) => {
					handleError(res, err);
				});
			//			this.awsActions.downloadImage(Config.bucketName, <string> imagekey, res);
		} catch (err) {
			handleError(res, err);
		}
	}

	public async listImages(req: Request, res: Response, next: NextFunction) {
		try {
			let path = req.params[0];
			this.manager.listImages(path)
				.then((ret) => {
					//					const responseAws = await this.awsActions.listImages(Config.bucketName, <string> path);
					res.json({ok: true, data: ret, message:""});
				})
				.catch((err) => {
					handleError(res, err);
				});
		} catch (err) {
			handleError(res, err);
		}
	}

	public async deleteImage(req: Request, res: Response, next: NextFunction) {
		try {
			let path = req.params[0];
			let file = req.params[1];
			this.manager.deleteImage(path + '/' + file)
				.then((ret) => {
					if (ret)
						res.json({ok: true});
					else
						res.json({error: true});
				})
				.catch((err) => {
					handleError(res, err);
				});

			//			const responseDelete = await this.awsActions.deleteImage(Config.bucketName, <string> imagekey);
		} catch (err) {
			handleError(res, err);
		}
	}

	/**
	 * Removes subfolders and images from the received item
	 * This endpoint should be used just from postman, not from the App client
	 */
	public async deleteFolder(req: Request, res: Response, next: NextFunction) {
		if (!Config.isGodMode)
			if (!checkRequiredRequestData(res, req, 'headers.type') || !checkRequiredRequestData(res, req, 'headers.iditem')) return;
		const {
			headers: {type, iditem}
		} = req;

		try {
			const folderPath: string = getFolderPath(<string> type, <string> iditem);
			const resultRemoveImages = await this.awsActions.removeFolder(Config.bucketName, folderPath);

			res.json(resultRemoveImages);
		} catch (err) {
			handleError(res, err);
		}
	}

	/**
	 * Update database task with the new assigned location
	 * @return the image of that location
	 */
	private async changeLocation(req: Request, res: Response, next: NextFunction) {
			if (!checkRequiredRequestData(res, req, 'body.floorPath') || !checkRequiredRequestData(res, req, 'body.taskId')) return;
		const {
			body: {floorPath, taskId}
		} = req;

		let floorId: ObjectId | string = this.getIdFromPath(floorPath);
		floorId = new ObjectId(floorId);

		try {
			// Update task location in database
			this.taskBackend.updateOne({location: floorId}, taskId);

			// Return image location of task
			const currentLocationImages: any[] = await this.manager.listImages(floorPath);
			//const currentLocationImages: any[] = (await this.awsActions.listImages(Config.bucketName, floorPath)).keys;
			if (currentLocationImages && currentLocationImages.length > 0) {
				this.manager.downloadImage(floorPath + '/' + currentLocationImages[0]).
				then((ret) => {
					res.attachment(<string> currentLocationImages[0]);
					ret.pipe(res);
				})
				.catch((err) => {
					handleError(res, err);
				});
				//this.awsActions.downloadImage(Config.bucketName, currentLocationImages[0], res);
			} else {
				res.json({ok: false});
			}
		} catch (err) {
			handleError(res, err);
		}
	}

	private getIdFromPath(path: string): string {
		return path.split('/')[1];
	}

	init() {
		this.router.post('/changelocation', this.auth.required, this.changeLocation.bind(this));
		this.router.post('/', this.auth.required, this.uploadImage.bind(this));
		this.router.get('/list/(*)$', this.auth.required, this.listImages.bind(this));
		this.router.get('/file/(*)/(*)$', this.auth.required, this.downloadImage.bind(this));
		this.router.delete('/deletefolder', this.auth.required, this.deleteFolder.bind(this));
		this.router.delete('/file/(*)/(*)$', this.auth.required, this.deleteImage.bind(this));
	}
}

const imageRoutes = new ImageRouter();
imageRoutes.init();

export default imageRoutes.router;
