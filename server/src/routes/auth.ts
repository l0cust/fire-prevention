import { Router, Request, Response, NextFunction } from 'express';
import { User } from './../models';
import { Auth } from './../auth/auth';
import * as passport from 'passport';

export class AuthRouter {
  router: Router;
  auth: Auth;

  constructor() {
    this.router = Router();
    this.auth = new Auth();
    this.init();
  }

  public async signIn(req: Request, res: Response, next: NextFunction) {
    const { body: { user } } = req;
    this.checkUserPass(res, user);

    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
      if (err) {
        return next(err);
      }
  
      if (passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();
        return res.json({ user: user.toAuthJSON() });
      }

      return res.json(info).status(400);
    })(req, res, next);
  }

  private checkUserPass(res: Response, user: User) {
    if (!user) {
      return this.handleError(res, 'User required');
    }
    if (!user.email) {
      return this.handleError(res, 'Email required');
    }
    if (!user.password) {
      return this.handleError(res, 'Password required');
    }
  }

  private handleError(res: Response, message: string): any {
    return res.status(422).json({ error: { message } });
  }

  init() {
    this.router.post('/signin', this.auth.optional, this.signIn.bind(this));
  }
}

const authRoutes = new AuthRouter();
authRoutes.init();

export default authRoutes.router;
