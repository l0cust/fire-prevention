import { Config } from '../config';
import { Router, Request, Response, NextFunction } from 'express';
import { Floor, CustomResponse, handleError, checkRequiredRequestData } from './../models';
import { FloorBackend, AwsActions } from './../backends';
import { Auth } from './../auth/auth';
import { TypeObjects, getFolderPath } from './../utils/utils';
import {IFileManager, LocalFileManager} from './../backends/file-manager';

export class FloorRouter {
  manager: IFileManager;
  router: Router;
  auth: Auth;
  floorBackend: FloorBackend;
  awsActions: AwsActions;

  constructor() {
    this.router = Router();
    this.auth = new Auth();
    this.floorBackend = new FloorBackend();
    if (Config.fileManager == "local") {
      this.manager = new LocalFileManager();
      this.manager.init({"root": Config.fileManagerRoot});
    }
    if (Config.fileManager == "aws") {
      // this.manager = new LocalFileManager({"root": Config.fileManagerRoot});
      // this.manager.init({"bucket": Config.bucketName, "endpoint": Config.bucketEndpoint, "id": Config.bucketKeyId, "secret": Config.bucketKeySecret});
      this.awsActions = new AwsActions();
    }
    this.init();
  }

  public async getFloors(req: Request, res: Response, next: NextFunction) {
    const resultQuery = await this.floorBackend.getAll();
    res.json(new CustomResponse(true, resultQuery).get());
  }

  public async getFloor(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'headers.floorid')) return;
    const {
      headers: { floorid }
    } = req;

    const resultQuery = await this.floorBackend.getOne(floorid);
    res.json(new CustomResponse(true, resultQuery).get());
  }

  public async getFloorsByCompany(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
    const {
      headers: { iditem }
    } = req;
    const resultQuery = await this.floorBackend.getByCompany(iditem);
    res.json(new CustomResponse(true, resultQuery).get());
  }

  public async createFloor(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.floor')) return;
    const {
      body: { floor }
    } = req;
    try {
      const floorInserted = await this.floorBackend.insertOne(floor);
      const fullFloorInserted = await this.floorBackend.getOne(floorInserted._id);

      res.json(new CustomResponse(true, fullFloorInserted, 'Floor created successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  /** Remove the images stored in the Cloud and if success, remove the floor from the database */
  public async deleteFloor(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'headers.iditem')) return;
    const {
      headers: { iditem }
    } = req;

    try {
      const folderPath: string = getFolderPath(TypeObjects.Floor, <string>iditem);
      const resultRemoveImages = await this.manager.removeFolder(folderPath);
      if (resultRemoveImages) {
        const deleteResponse = await this.floorBackend.deleteOne(iditem);
        res.json(new CustomResponse(true, new Floor(deleteResponse), 'Floor deleted successfully').get());
      } else {
        throw Error('Error removing the images from the cloud');
      }
    } catch (err) {
      if (err.code === 'ENOENT') {
        const deleteResponse = await this.floorBackend.deleteOne(iditem);
        res.json(new CustomResponse(true, new Floor(deleteResponse), 'Floor deleted successfully').get());
      } else {
        handleError(res, err);
      }
    }
  }

  public async updateFloor(req: Request, res: Response, next: NextFunction) {
    if (!checkRequiredRequestData(res, req, 'body.floor') || !checkRequiredRequestData(res, req, 'body.floor._id')) {
      return;
    }
    const {
      body: { floor }
    } = req;
    const _id = floor._id;

    try {
      const floorUpdated = await this.floorBackend.updateOne(new Floor(floor), _id);
      const fullFloorUpdate = await this.floorBackend.getOne(floorUpdated._id);
      res.json(new CustomResponse(true, fullFloorUpdate, 'Floor updated successfully').get());
    } catch (err) {
      handleError(res, err);
    }
  }

  init() {
    this.router.get('/company', this.auth.required, this.getFloorsByCompany.bind(this));
    this.router.get('/floorid', this.auth.required, this.getFloor.bind(this));
    this.router.get('/', this.auth.required, this.getFloors.bind(this));
    this.router.post('/', this.auth.required, this.createFloor.bind(this));
    this.router.delete('/', this.auth.required, this.deleteFloor.bind(this));
    this.router.patch('/', this.auth.required, this.updateFloor.bind(this));
  }
}

const floorRoutes = new FloorRouter();
floorRoutes.init();

export default floorRoutes.router;
