import { Component } from '@angular/core';
import { routerFadeAnimation } from '@shared/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routerFadeAnimation]
})
export class AppComponent {
  title = 'app';
}
