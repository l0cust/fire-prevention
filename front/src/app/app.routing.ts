import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { IsLogedGuard } from '@shared/guards';

// Routes to different modules
const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  {
    path: 'task-manager',
    loadChildren: './task-manager/task-manager.module#TaskManagerModule',
    runGuardsAndResolvers: 'always',
    canActivate: [IsLogedGuard]
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
