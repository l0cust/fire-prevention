import { environment } from '@env/environment';

export enum Type {
  User = 'User',
  Company = 'Company',
  Floor = 'Floor',
  CategoryTask = 'CategoryTask',
  Task = 'Task'
}

export enum DetailMode {
  Edit = 'edit',
  Create = 'create',
  View = 'view'
}

export enum MagicStrings {
  DetailItem = 'detail-item',
  NestedList = 'nested-list',
  UserLogedDev = 'user-fire-dev',
  UserLogedProd = 'user-fire-prod',
  NameApp = 'fire-prevention'
}

/** @return NameApp depend of dev-prod environment */
export function getNameApp(): string {
  return environment.production ? MagicStrings.NameApp : `${MagicStrings.NameApp}-dev`;
}

export enum SectionsImage {
  Location = 'location',
  After = 'after',
  Before = 'before'
}

export enum Roles {
  Admin = 'admin',
  Employee = 'employee'
}

export function userLogedKey(): string {
  return environment.production ? MagicStrings.UserLogedProd : MagicStrings.UserLogedDev;
}
