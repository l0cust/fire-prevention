import { Item } from './Item';

export class CategoryTask extends Item {
  static availableTypes: string[] = ['Type 1', 'Type 2', 'Type 3', 'Type 4', 'Type 5', 'Type 6'];
  static availableMeasures: number[] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
  static defaultColor: string = '#bbb';

  color: string;
  hasType: boolean;
  hasMeasure: boolean;
  hasAmount: boolean;

  constructor(json: any) {
    if (json) {
      super(json.name, json._id);
      this.color = json.color || CategoryTask.defaultColor;
      this.hasType = json.hasType;
      this.hasMeasure = json.hasMeasure;
      this.hasAmount = json.hasAmount;
    } else {
      this.color = CategoryTask.defaultColor;
    }
  }
}
