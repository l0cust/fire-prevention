import { Item } from './Item';
import { User } from './User';
import { Floor } from './Floor';
import { CategoryTask } from './CategoryTask';
import { PositionMarker } from './PositionMarker';

export class Task extends Item {
  description?: string;
  madeBy: User;
  photosBefore?: string[];
  photosAfter?: string[];
  location?: Floor;
  categoryTask?: CategoryTask;
  positionMarker?: PositionMarker;
  type: string;
  measure: number;
  amount: number;

  constructor(json: any = {}) {
    if (json) {
      super(json.name, json._id);
      this.description = json.description;
      this.madeBy = new User(json.madeBy);
      this.photosBefore = json.photosBefore;
      this.photosAfter = json.photosAfter;
      if (json.location) {
        this.location = new Floor(json.location);
      }
      if (json.categoryTask) {
        this.categoryTask = new CategoryTask(json.categoryTask);
      }
      this.positionMarker = new PositionMarker(json.positionMarker);
      this.type = json.type;
      this.measure = json.measure;
      this.amount = json.amount;
    }
  }

  /** @return the color of the associate categoryTaks, or the default color if categoryTask is not defined */
  getTypeColor(): string {
    return this.categoryTask ? this.categoryTask.color : CategoryTask.defaultColor;
  }

  /** @return a Tag ID compounded of the location name and the task._id */
  getTagId(): string {
    return Task.getTagId(this);
  }

  /**
   * Return a Tag ID compounded of the location name and the task._id
   * @param task to get the Tag ID
   */
  static getTagId(task: Task): string {
    if (!task) {
      return '';
    }
    if (task.location && task.location.name && task._id) {
      return `${task.location.name}_${Task.getTagNumber(task._id)}`;
    }
    return task.name || '';
  }

  /**
   * Apply the desired modification to the Id, normally, shorts it
   * @param id of the tasks to perform
   */
  static getTagNumber(id: string): string {
    return id ? id.slice(-5) : '';
  }
}
