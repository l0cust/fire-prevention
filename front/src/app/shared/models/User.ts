import { Item } from './Item';
import { Roles, userLogedKey } from './CustomEnums';

export class User extends Item {
  email: string;
  role: string;
  token?: string;

  constructor(json: any = {}) {
    if (json) {
      super(json.name, json._id);
      this.email = json.email;
      this.role = json.role;
      if (json.token) {
        this.token = json.token;
      }
    }
  }

  isAdmin(): boolean {
    return this.role === Roles.Admin;
  }

  static isAdmin() {
    const userStored: string = localStorage.getItem(userLogedKey());
    if (userStored !== 'undefined' && userStored) {
      return JSON.parse(userStored).role === Roles.Admin;
    }
    return false;
  }
}
