export class PositionMarker {
  x: number;
  y: number;
  width: number;
  height: number;

  constructor(json?) {
    if (json) {
      this.x = json.x;
      this.y = json.y;
      this.width = json.width;
      this.height = json.height;
    }
  }
}
