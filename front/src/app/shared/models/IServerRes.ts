export interface IServerRes {
  ok: boolean;
  data: any;
  message: string;
}
