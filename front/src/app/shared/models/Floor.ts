import { Item } from './Item';
import { Company } from './Company';

export class Floor extends Item {
  plane: string;
  original: boolean;
  company?: Company;

  constructor(json: any) {
    if (json) {
      super(json.name, json._id);
      this.plane = json.img;
      this.original = json.original ? true : false;
      this.company = json.company ? new Company(json.company) : null;
    }
  }
}
