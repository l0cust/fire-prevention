export class Item {
  name: string;
  _id: string;

  constructor(name: string, _id: string) {
    this.name = name;
    this._id = _id;
  }
}
