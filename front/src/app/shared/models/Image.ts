import { Observable } from 'rxjs';
import { PositionMarker } from './PositionMarker';

export class ImageFire {
  name: string;
  path: string;
  imageObj;
  width: number;
  height: number;

  constructor(json?) {
    if (json) {
      this.name = json.name;
      this.path = json.path;
    }
  }

  static getPercentPos(xClick: number, yClick: number, xImgContainer: number, yImgContainer): PositionMarker {
    const xPercent: number = (100 * xClick) / xImgContainer;
    const yPercent: number = (100 * yClick) / yImgContainer;
    const pMarker: PositionMarker = new PositionMarker({ x: xPercent, y: yPercent });
    return pMarker;
  }

  setHeightAndWidth() {
    const img = new Image();
    img.onload = () => {
      this.width = img.width;
      this.height = img.height;
    };
    img.src = this.imageObj;
  }

  setImageObj(image: Blob, observer?) {
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.imageObj = reader.result;
        this.setHeightAndWidth();
        if (observer) {
          observer.next(true);
        }
      },
      false
    );

    if (image) {
      reader.readAsDataURL(image);
    } else {
      if (observer) {
        observer.next(false);
      }
    }
  }

  setImageObjAndNotifiy(image: Blob): Observable<boolean> {
    return Observable.create(observer => {
      return this.setImageObj(image, observer);
    });
  }

  /**
   * Convert image from base64 to blob
   * @param dataURI image in base64
   */
  static dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    const mimeString = dataURI
      .split(',')[0]
      .split(':')[1]
      .split(';')[0];
    // write the bytes of the string to a typed array
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeString });
  }
}
