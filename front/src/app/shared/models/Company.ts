import { Item } from './Item';

export class Company extends Item {
  constructor(json: any = {}) {
    if (json) {
      super(json.name, json._id);
    }
  }
}
