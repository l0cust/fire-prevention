export class Marker {
  Sprite;
  Width: number;
  Height: number;
  XPos: number;
  YPos: number;

  constructor() {
    this.Sprite = new Image();
    this.Sprite.src = 'http://www.clker.com/cliparts/w/O/e/P/x/i/map-marker-hi.png';
    this.Width = 12;
    this.Height = 20;
    this.XPos = 0;
    this.YPos = 0;
  }
}
