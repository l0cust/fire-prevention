import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DetailMode, IImageToUpload, ImageFire } from '@shared/models';

@Component({
  selector: 'image-element',
  templateUrl: './image-element.component.html',
  styleUrls: ['./image-element.component.scss']
})
export class ImageElementComponent {
  DetailMode = DetailMode;
  @Input()
  image: ImageFire = null;
  @Input()
  imageToUpload: IImageToUpload = null;
  @Input()
  mode: string;
  @Input()
  isLocation: boolean = false;
  @Output()
  delete: EventEmitter<any> = new EventEmitter();
  @Output()
  editLocation: EventEmitter<any> = new EventEmitter();

  constructor() {}

  isLoading(): boolean {
    return (this.image && this.image.imageObj) || (this.imageToUpload && this.imageToUpload.imgUrl);
  }

  showDeleteIcon(): boolean {
    if (this.isLocation) {
      return false;
    }
    return this.mode === DetailMode.Edit || this.mode === DetailMode.Create;
  }
}
