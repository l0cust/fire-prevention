import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { DetailMode, IImageToUpload, Type, ImageFire } from '@shared/models';
import { ImageService } from '@task-manager/services';
import { DialogComponent } from '@shared/components/dialog.component';
import { MatDialog } from '@angular/material';
import { Observable, of } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'image-section',
  templateUrl: './image-section.component.html',
  styleUrls: ['./image-section.component.scss']
})
export class ImageSectionComponent implements AfterViewInit {
  DetailMode = DetailMode;
  @Input()
  itemId: string;
  @Input()
  mode: string;
  @Input()
  sectionImage: string;
  @Input()
  type: string;
  @Input()
  allowMultipleSelection: boolean = true;
  imagesToUpload: IImageToUpload[] = [];
  listedImages: ImageFire[] = [];
  LIMIT_MB = 256;
  showSpinner: boolean = false;

  constructor(private imageService: ImageService, public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.getImages();
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {
      if (this.type === Type.Floor) {
        this.imagesToUpload = [];
      }
      Array.from(event.target.files).forEach((file: any) => {
        if (file.size > 1024 * 1024 * this.LIMIT_MB) {
          DialogComponent.openDialog(this.dialog, `File cannot be bigger than ${this.LIMIT_MB} Mb`);
          return;
        }
        if (this.listedImages.filter((image: ImageFire) => image.name === file.name).length > 0) {
          DialogComponent.openDialog(this.dialog, `The image file named ${file.name} is already loaded into the gallery. Choose another image to upload.`);
          return;
        }
        const img: IImageToUpload = { image: file };
        this.imagesToUpload.push(img);
        const reader = new FileReader();
        reader.onload = (e: any) => (img.imgUrl = e.target.result);
        reader.readAsDataURL(file);
      });
    }
  }

  /** This function was called from the previous button that handle the saving from this component
   *  Now is deprecated and the function will be removed in the future.
   */
  saveImages() {
    if (this.type === Type.Floor) {
      this.removePreviousImagesAndUpload(this.itemId).subscribe();
    } else {
      this.uploadImages(this.itemId).subscribe(
        res => {},
        err => DialogComponent.openDialog(this.dialog, 'Error uploading the images')
      );
    }
  }

  removePreviousImagesAndUpload(itemId: string, informResult: boolean = true): Observable<any> {
    this.itemId = itemId;
    if (this.listedImages.length > 0) {
      return DialogComponent.openDialog(
        this.dialog,
        `This image will override the previous one. Are you sure about overriding the previous one?`,
        true
      ).pipe(
        switchMap(result => {
          if (result) {
            return this.imageService.deleteImage(this.listedImages[0].path, this.listedImages[0].name).pipe(
              switchMap(res => {
                if (res.ok) {
                  return this.uploadImages(this.itemId, informResult);
                }
              })
            );
          }
        })
      );
    }
    return this.uploadImages(this.itemId);
  }

  uploadImages(itemId: string, informResult: boolean = true): Observable<any> {
    this.itemId = itemId;
    let path: string = (this.type === Type.Task)? `${this.type}/${this.itemId}/${this.sectionImage}` : `${this.type}/${this.itemId}`;
    if (!this.imagesToUpload || this.imagesToUpload.length < 1) return of({});
    this.showSpinner = true;
    return this.imageService
      .uploadMultipleImages(this.imagesToUpload, path)
      .pipe(
        tap((result: boolean) => {
          if (result) {
            this.imagesToUpload = [];
            this.getImages();
            informResult && DialogComponent.openDialog(this.dialog, 'All the images have been uploaded successfully');
          } else {
            DialogComponent.openDialog(this.dialog, 'Error uploading the images');
          }
          this.showSpinner = false;
        })
      );
  }

  getImages() {
    let path: string = (this.type === Type.Task)? `${this.type}/${this.itemId}/${this.sectionImage}` : `${this.type}/${this.itemId}`;
    if (this.itemId && this.itemId.length > 0) {
      this.imageService.listImages(path).subscribe((res: ImageFire[]) => {
        this.listedImages = res;
        this.listedImages.forEach((image: ImageFire) => {
          this.imageService.getImage(image.path, image.name).subscribe((res: Blob) => {
            image.setImageObj(res);
          });
        });
      });
    }
  }

  removeImage(path: string, name: string) {
    DialogComponent.openDialog(
      this.dialog,
      `Are you sure that you want to delete this image? You won't be able to recover the image`,
      true
    ).subscribe(result => {
      if (result) {
        this.showSpinner = true;
        this.imageService.deleteImage(path, name).subscribe(
          res => {
            if (res.ok) {
              DialogComponent.openDialog(this.dialog, 'Image deleted successfully');
              this.listedImages.splice(this.listedImages.findIndex((img: ImageFire) => img.name === name && img.path === path), 1);
            }
            this.showSpinner = false;
          },
          err => this.handleError(err),
          () => (this.showSpinner = false)
        );
      }
    });
  }

  handleError(err) {
    console.error(err);
  }

  deleteImageToUpload(image: IImageToUpload) {
    this.imagesToUpload.splice(this.imagesToUpload.findIndex((img: IImageToUpload) => img.imgUrl === image.imgUrl), 1);
  }
}
