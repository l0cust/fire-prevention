import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'custom-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss']
})
export class MultiSelectComponent {
  @Input() disableSelect: boolean = false;
  @Input() myOptions: any[] = [];
  @Input() selectedValues: string[] = [];
  @Output() selectedChanged: EventEmitter<any[]> = new EventEmitter<any[]>();

  compareWithFnOpt(a: any, b: any) {
    return a.name === b.name;
  }

  selectionChange() {
    this.selectedChanged.emit(this.selectedValues);
  }
}
