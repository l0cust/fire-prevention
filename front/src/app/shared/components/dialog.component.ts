import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
@Component({
  selector: 'custom-dialog',
  template: `
  <div *ngIf="data">
    <mat-dialog-content *ngIf="data.content">{{ data.content }}</mat-dialog-content>
    <mat-dialog-actions>
      <button *ngIf="data.cancelButton" (click)="dialogRef.close(false)" mat-button>Cancel</button>
      <button *ngIf="data.okButton" (click)="dialogRef.close(true)" mat-button>Ok</button>
    </mat-dialog-actions>
  </div>
  `,
  styles: [
    `
      div {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
    `
  ]
})
export class DialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<DialogComponent>) {}

  static openDialog(
    dialog: MatDialog,
    content: string,
    cancelButton: boolean = false,
    okButton: boolean = true
  ): Observable<boolean> {
    const dialogRef = dialog.open(DialogComponent, {
      data: {
        content,
        okButton,
        cancelButton
      },
      minWidth: '400px'
    });
    return dialogRef.afterClosed();
  }
}
