import { Component, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { Task, PositionMarker, ImageFire, DetailMode } from '@shared/models';
import { get } from 'lodash';

enum NamesHtml {
  imageWrapperId = 'imageContainer',
  markerWrapper = 'marker-wrapper',
  markerLegend = 'marker-legend',
  markerIcon = 'marker-html',
  noLegend = 'no-legend'
}

@Component({
  selector: 'image-location',
  templateUrl: './image-location.component.html',
  styleUrls: ['./image-location.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ImageLocationComponent {
  DetailMode = DetailMode;

  @Input()
  task: Task;
  @Input()
  imageLocation: ImageFire;
  @Input()
  mode: string;
  @Input()
  isInDetailTask: boolean = false;
  @Input()
  relatedTasks: any[] = null;
  @Output()
  markerClicked: EventEmitter<Task> = new EventEmitter();

  @ViewChild('imageLocationTag')
  imageLocationTag;

  originalPositionMarker: PositionMarker;
  isLoading: boolean = false;

  constructor() {}

  init() {
    if (this.task) {
      this.drawMarker();
    }
  }

  startLoading() {
    this.isLoading = true;
  }

  stopLoading() {
    this.isLoading = false;
    this.init();
  }

  drawRelatedTasks(tasks: Task[]) {
    this.removeMarkersDrawn();
    tasks.forEach(t => {
      this.drawMarker(t);
    });
  }

  refreshMarker() {
    this.removeMarkersDrawn();
    this.drawMarker();
  }

  drawMarker(taskToDraw: Task = this.task) {
    if (taskToDraw.positionMarker && Object.keys(taskToDraw.positionMarker).length) {
      if (this.isInDetailTask) {
        this.originalPositionMarker = { ...this.task.positionMarker };
      }
      this.buildHtmlMarker(taskToDraw);
    }
  }

  clickImage(event) {
    if (this.mode !== DetailMode.Edit && this.mode !== DetailMode.Create) {
      return;
    }

    const pos_x = event.offsetX ? event.offsetX : event.pageX - this.imageLocationTag.nativeElement.offsetLeft;
    const pos_y = event.offsetY ? event.offsetY : event.pageY - this.imageLocationTag.nativeElement.offsetTop;

    this.removePreviousMarkers();

    // use the size of the img tab as long as the size of the image could be bigger than the tag (max-size in the css)
    const xImgContainer: number = this.imageLocationTag.nativeElement.clientWidth;
    const yImgContainer: number = this.imageLocationTag.nativeElement.clientHeight;

    const pMarker: PositionMarker = ImageFire.getPercentPos(pos_x, pos_y, xImgContainer, yImgContainer);
    this.task.positionMarker = pMarker;
    this.buildHtmlMarker(this.task);
  }

  removeMarkersDrawn() {
    Array.from(document.getElementsByClassName(NamesHtml.markerWrapper)).forEach(marker => {
      marker.parentNode.removeChild(marker);
    });
  }
  removePreviousMarkers() {
    this.removeMarkersDrawn();
    this.task.positionMarker = null;
  }

  buildHtmlMarker(taskToDraw: Task) {
    if (!get(taskToDraw, 'positionMarker.y') || !get(taskToDraw, 'positionMarker.x')) {
      return;
    }
    const markerWrapper = document.createElement('div');
    markerWrapper.classList.add(NamesHtml.markerWrapper);
    if (!taskToDraw._id) {
      markerWrapper.classList.add(NamesHtml.noLegend);
    }
    markerWrapper.style.position = 'absolute';
    markerWrapper.style.top = `${taskToDraw.positionMarker.y}%`;
    markerWrapper.style.left = `${taskToDraw.positionMarker.x}%`;

    const icon = document.createElement('i');
    icon.innerHTML = 'location_on'; // set position icon marker
    icon.classList.add(NamesHtml.markerIcon);
    icon.classList.add('material-icons'); // make the i tag display material-icon
    icon.style.color = taskToDraw.getTypeColor();

    // This make posible add and event click and get the item in through that event
    icon.addEventListener('click', this.onClickMarker.bind(this));
    Object.defineProperty(icon, 'item', { value: taskToDraw });

    if (taskToDraw._id) {
      this.addLegend(markerWrapper, taskToDraw);
    }

    markerWrapper.appendChild(icon);
    setTimeout(() => {
      const imagWrapper = document.getElementById(NamesHtml.imageWrapperId);
      if (imagWrapper) {
        imagWrapper.appendChild(markerWrapper);
      } else {
        setTimeout(() => this.init(), 200);
      }
    }, 50);
  }

  addLegend(markerWrapper: HTMLDivElement, taskToDraw: Task) {
    const box = document.createElement('div');
    box.innerHTML = Task.getTagNumber(taskToDraw._id) || '';
    box.classList.add(NamesHtml.markerLegend);
    markerWrapper.appendChild(box);
  }

  onClickMarker(event) {
    const objectClicked: Task = event.target.item;
    this.markerClicked.emit(objectClicked);
  }
}
