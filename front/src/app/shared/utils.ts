import { CategoryTask, Task } from '@shared/models';

/**
 * Filter array of tasks by tasks category.
 * If receives the filteredArray, return the same object (to keep the pointer)
 * @param selectedTypeTasks typeTask to filter by
 * @param arrToFilter array of tasks to be filtered
 * @param arrFiltered array of tasks being filtered
 */
export function filterByTaskCategory(
  selectedTypeTasks: CategoryTask[],
  arrToFilter: Task[],
  arrFiltered: Task[] = []
): Task[] {
  arrFiltered.splice(0, arrFiltered.length);
  if (selectedTypeTasks.length) {
    arrFiltered.push(
      ...arrToFilter.filter((t: Task) => {
        return selectedTypeTasks.find((cat: CategoryTask) => t.categoryTask._id === cat._id);
      })
    );
  } else {
    arrFiltered.push(...arrToFilter);
  }

  return arrFiltered;
}
