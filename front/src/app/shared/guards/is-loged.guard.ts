import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { userLogedKey } from './../models';
import { JwtHelperService } from '@auth0/angular-jwt';

/**
 * Return true if there is a stored and valid token
 */
@Injectable({
  providedIn: 'root'
})
export class IsLogedGuard implements CanActivate {
  constructor(public router: Router, public jwtHelper: JwtHelperService) {}

  canActivate(): boolean {
    const userStored: string = localStorage.getItem(userLogedKey());
    let isValidToken: boolean = false;

    if (userStored !== 'undefined' && userStored) {
      const token = JSON.parse(userStored).token;
      isValidToken = !this.jwtHelper.isTokenExpired(token);
    }

    if (!isValidToken) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
