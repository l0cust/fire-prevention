import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import decode from 'jwt-decode'; // tslint:disable-line
import { userLogedKey } from '../models';

/**
 * Return true if the token contains the expected role
 */
@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data.expectedRole;

    const userStored: string = localStorage.getItem(userLogedKey());

    if (userStored === 'undefined' || !userStored) {
      return false;
    }

    const token = JSON.parse(userStored).token;
    const tokenPayload = decode(token);
    if (tokenPayload.role !== expectedRole) {
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }
}
