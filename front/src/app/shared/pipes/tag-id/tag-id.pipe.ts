import { Pipe, PipeTransform } from '@angular/core';
import { Task, Item } from './../../models';
/**
 * Display the tagId for tasks, and name for other items.
 * It uses a boolean to not trust on typeof
 */
@Pipe({
  name: 'tagId'
})
export class TagIdPipe implements PipeTransform {
  transform(task: Item, isTask: boolean = true): string {
    if (isTask) {
      return Task.getTagId(<Task>task);
    }
    return task.name;
  }
}
