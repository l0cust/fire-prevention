import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) {}

  public signin(email: string, password: string): Observable<any> {
    return this.http.post(`${environment.URL}/auth/signin`, { user: { email, password } });
  }
}
