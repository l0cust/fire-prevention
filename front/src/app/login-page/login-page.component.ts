import { Component, OnInit } from '@angular/core';
import { User, userLogedKey } from '@shared/models';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '@shared/components/dialog.component';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  email: string;
  password: string;
  errorText: string;

  constructor(private router: Router, private loginService: LoginService, public dialog: MatDialog) {}

  ngOnInit() {}

  login() {
    this.errorText = '';
    this.loginService.signin(this.email, this.password).subscribe(
      res => {
        if (res.error) {
          this.errorText = res.error.message || res.error;
        } else {
          localStorage.setItem(userLogedKey(), JSON.stringify(new User(res.user)));
          if (this.password.includes('admin')) {
            DialogComponent.openDialog(this.dialog, `Warning! You are probably signed in to the default user or your password is unsafe. Please change your password after successfull sign in.`);
          }
          this.router.navigate(['task-manager']);
        }
      },
      err => {
        console.error(err);
        this.errorText = 'Error trying to login.';
      }
    );
  }
}
