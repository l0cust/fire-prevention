import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { LoginPageComponent } from './login-page/login-page.component';
import { SharedModule } from '@shared/shared.module';

import { userLogedKey } from '@shared/models';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

export function tokenGetter() {
  const userStored: string = localStorage.getItem(userLogedKey());
  if (userStored !== 'undefined' && userStored) {
    return JSON.parse(userStored).token;
  }
  return false;
}

@NgModule({
  declarations: [AppComponent, LoginPageComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        // Add here the routes where the token should be automatically injected to the http request
        whitelistedDomains: ['localhost:3000', 'www.digitalsolutions-sei.com', 'digitalsolutions-sei.con', '165.22.127.72:3000'],
        blacklistedRoutes: [
          'http://localhost:3000/api/auth/signin',
          'www.digitalsolutions-sei.com/fire-prevention-server/auth/signin',
          'http://www.digitalsolutions-sei.com/fire-prevention-server/auth/signin',
          'https://www.digitalsolutions-sei.com/fire-prevention-server/auth/signin',
          'digitalsolutions-sei.com/fire-prevention-server/auth/signin',
          'http://digitalsolutions-sei.com/fire-prevention-server/auth/signin',
          'https://digitalsolutions-sei.com/fire-prevention-server/auth/signin',
          'http://165.22.127.72:3000/api/auth/signin',
          'https://165.22.127.72:3000/api/auth/signin'
        ]
      }
    })
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule {}
