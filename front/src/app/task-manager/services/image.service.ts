import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';
import { IImageToUpload, Type, ImageFire, SectionsImage, getNameApp } from '@shared/models';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  constructor(private http: HttpClient) {}

  uploadImage(image: IImageToUpload, path: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`${path}`, image.image);
    return this.http.post(`${environment.URL}/image`, formData, { reportProgress: true });
  }

  uploadMultipleImages(images: IImageToUpload[], path: string): Observable<boolean> {
    const imagesUp$: Observable<any>[] = images.map(image => this.uploadImage(image, path));
    return forkJoin(imagesUp$).pipe(
      map((res: { ok: boolean; message: string; }[]) => {
        const result: boolean = res.reduce((acc, val) => val.ok, true);
        return result;
      })
    );
  }

  listImages(path: string): Observable<ImageFire[]> {
    return this.http.get(`${environment.URL}/image/list/${path}`).pipe(
      map((res: any) => res.data),
      map(data => {
        const imgs: ImageFire[] = data.map(entry => new ImageFire({ name : entry, path }));
        return imgs;
      })
    );
  }

  getImage(path: string, name: string): Observable<Blob> {
    return this.http.get(`${environment.URL}/image/file/${path}/${name}`, { responseType: 'blob' });
  }

  deleteImage(path: string, name: string): Observable<any> {
    return this.http.delete(`${environment.URL}/image/file/${path}/${name}`);
  }

  changeLocation(floorId: string, taskId: string) {
    const floorPath: string = `${Type.Floor}/${floorId}`;
    return this.http.post(`${environment.URL}/image/changelocation`, { floorPath, taskId }, { responseType: 'blob' });
  }
}
