import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Company } from '@shared/models';
import { environment } from '@env/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  constructor(private http: HttpClient) {}

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(`${environment.URL}/company`).pipe(map((res: any) => (res.ok ? res.data : [])));
  }

  create(company: Company): Observable<any> {
    return this.http.post(`${environment.URL}/company`, { company });
  }

  edit(company: Company): Observable<any> {
    return this.http.patch(`${environment.URL}/company`, { company });
  }

  delete(company: Company): Observable<any> {
    const headers = new HttpHeaders().set('iditem', company._id);
    return this.http.delete(`${environment.URL}/company`, { headers });
  }
}
