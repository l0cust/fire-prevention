import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User, MagicStrings, userLogedKey } from '@shared/models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userLoged: User;  // here the user loged is store to be more handy cross platform

  constructor(private router: Router, private http: HttpClient) {
    this.loadLoged();
  }

  setUserLoged(user: User = this.userLoged) {
    const itemStored = localStorage.getItem(userLogedKey());
    if (itemStored !== 'undefined' && itemStored) {
      // to avoid override the stored token
      this.userLoged = new User({ ...JSON.parse(itemStored), ...user });
      localStorage.setItem(userLogedKey(), JSON.stringify(this.userLoged));
    } else {
      localStorage.setItem(userLogedKey(), JSON.stringify(user));
    }
  }

  loadLoged() {
    if (!this.userLoged) {
      const itemStored = localStorage.getItem(MagicStrings.DetailItem);
      if (itemStored !== 'undefined' && itemStored) {
        this.userLoged = new User(JSON.parse(itemStored));
      }
    }
  }

  /** Password update has an special route to handle more restrictive permissions */
  updatePassword(id: string, password: string) {
    return this.http.patch<User[] | any>(`${environment.URL}/user/password`, { id, password });
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.URL}/user`).pipe(map((res: any) => (res.ok ? res.data : [])));
  }

  create(user: User): Observable<any> {
    return this.http.post(`${environment.URL}/user`, { user });
  }

  edit(user: User): Observable<any> {
    return this.http.patch(`${environment.URL}/user`, { user });
  }

  delete(user: User): Observable<any> {
    const headers = new HttpHeaders().set('iditem', user._id);
    return this.http.delete(`${environment.URL}/user`, { headers });
  }

  logOut() {
    this.userLoged = null;
    localStorage.removeItem(userLogedKey());
    this.router.navigate(['login']);
  }

  userIsAdmin(): boolean {
    return this.userLoged.isAdmin();
  }
}
