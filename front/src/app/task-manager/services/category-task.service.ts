import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CategoryTask } from '@shared/models';
import { environment } from '@env/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryTaskService {
  constructor(private http: HttpClient) {}

  getCategoryTasks(): Observable<CategoryTask[]> {
    return this.http
      .get<CategoryTask[]>(`${environment.URL}/categorytask`)
      .pipe(map((res: any) => (res.ok ? res.data : [])));
  }

  create(categorytask: CategoryTask): Observable<any> {
    return this.http.post(`${environment.URL}/categorytask`, { categorytask });
  }

  edit(categorytask: CategoryTask): Observable<any> {
    return this.http.patch(`${environment.URL}/categorytask`, { categorytask });
  }

  delete(categorytask: CategoryTask): Observable<any> {
    const headers = new HttpHeaders().set('iditem', categorytask._id);
    return this.http.delete(`${environment.URL}/categorytask`, { headers });
  }
}
