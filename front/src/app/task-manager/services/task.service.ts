import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Task } from '@shared/models';
import { environment } from '@env/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private http: HttpClient) {}

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(`${environment.URL}/task`).pipe(map((res: any) => (res.ok ? res.data : [])));
  }

  getTask(taskid: string): Observable<Task[]> {
    const headers = new HttpHeaders().set('taskid', taskid);
    return this.http
      .get<Task[]>(`${environment.URL}/taskid`, { headers })
      .pipe(map((res: any) => (res.ok ? res.data : [])));
  }

  /** Get tasks related to a floor */
  getByFloor(floorId: string): Observable<Task[]> {
    if (floorId) {
      const headers = new HttpHeaders().set('iditem', floorId);
      return this.http
        .get<Task[]>(`${environment.URL}/task/floor`, { headers })
        .pipe(map((res: any) => (res.ok ? res.data : [])));
    }
    return of([]);
  }

  create(task: Task, images: any[]): Observable<any> {
    return this.http.post(`${environment.URL}/task`, { task, images });
  }

  edit(task: Task, images: any[]): Observable<any> {
    return this.http.patch(`${environment.URL}/task`, { task, images });
  }

  delete(task: Task): Observable<any> {
    const headers = new HttpHeaders().set('iditem', task._id);
    return this.http.delete(`${environment.URL}/task`, { headers });
  }
}
