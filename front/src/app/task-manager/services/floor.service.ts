import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Floor } from '@shared/models';
import { environment } from '@env/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FloorService {
  constructor(private http: HttpClient) {}

  getFloors(): Observable<Floor[]> {
    return this.http.get<Floor[]>(`${environment.URL}/floor`).pipe(map((res: any) => (res.ok ? res.data : [])));
  }
  getByCompany(companyId: string): Observable<Floor[]> {
    if (companyId) {
      const headers = new HttpHeaders().set('iditem', companyId);
      return this.http
        .get<Floor[]>(`${environment.URL}/floor/company`, { headers })
        .pipe(map((res: any) => (res.ok ? res.data : [])));
    }
    of([]);
  }

  create(floor: Floor): Observable<any> {
    return this.http.post(`${environment.URL}/floor`, { floor });
  }

  edit(floor: Floor): Observable<any> {
    return this.http.patch(`${environment.URL}/floor`, { floor });
  }

  delete(floor: Floor): Observable<any> {
    const headers = new HttpHeaders().set('iditem', floor._id);
    return this.http.delete(`${environment.URL}/floor`, { headers });
  }
}
