import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { TaskManagerRoutingModule } from './task-manager.routing';
import { ColorPickerModule } from 'ngx-color-picker';

import {
  MainToolbarComponent,
  ListingPageComponent,
  DetailUserComponent,
  DetailTaskComponent,
  DetailCompanyComponent,
  DetailFloorComponent,
  DetailPageComponent,
  DetailCategoryTaskComponent,
  ReportingComponent,
  SidenavComponent
} from './components';

@NgModule({
  imports: [CommonModule, SharedModule, TaskManagerRoutingModule, FormsModule, ColorPickerModule],
  declarations: [
    SidenavComponent,
    MainToolbarComponent,
    ListingPageComponent,
    DetailUserComponent,
    DetailTaskComponent,
    DetailCompanyComponent,
    DetailFloorComponent,
    DetailPageComponent,
    DetailCategoryTaskComponent,
    ReportingComponent
  ]
})
export class TaskManagerModule {}
