import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SidenavComponent, ListingPageComponent, DetailPageComponent } from './components';
import { ReportingComponent } from './components/reporting/reporting.component';

// Here are specified the routes of this module. All of them are children on the task-manager
const routes: Routes = [
  {
    path: '',
    component: SidenavComponent,
    children: [
      { path: 'listing/:type', component: ListingPageComponent },
      { path: 'listing/:type/detail/:mode', component: DetailPageComponent, runGuardsAndResolvers: 'always' },
      { path: 'reporting/excel', component: ReportingComponent },
      { path: '', redirectTo: 'listing/Task' }
    ],
    runGuardsAndResolvers: 'always'
  },
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskManagerRoutingModule {}
