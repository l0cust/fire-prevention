import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';
import * as xlsx from 'xlsx';
import { Task } from '@shared/models';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() {}

  createReport(data: Task[]) {
    let reportingData: any[] = data.map((task: Task) => ({
      "tag id" : Task.getTagId(task),
      "description" : (task.description && task.description.length > 0)? task.description : "",
      "width" : (task.positionMarker && task.positionMarker.width !== undefined)? task.positionMarker.width : 0,
      "height" : (task.positionMarker && task.positionMarker.height !== undefined)? task.positionMarker.height : 0,
      "area" : (task.measure && task.measure !== undefined)? task.measure : 0,
      "number" : (task.amount && task.amount !== undefined)? task.amount : 0,
      "rates bracket" : ""
    }));
    let workbook: xlsx.WorkBook = xlsx.utils.book_new();
    let sheet: xlsx.WorkSheet = xlsx.utils.json_to_sheet(reportingData);
    let sheetName: string = 'Report_';
    let currentDate: Date = new Date();
    sheetName += currentDate.getFullYear() + '-';
    sheetName += (currentDate.getMonth()+1 < 10)? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1);
    sheetName += (currentDate.getDate() < 10)? '-0' + currentDate.getDate() : '-' + currentDate.getDate();
    sheetName += '_' + ((currentDate.getHours() < 10)? '0' + currentDate.getHours() : currentDate.getHours());
    sheetName += '.' + ((currentDate.getMinutes() < 10)? '0' + currentDate.getMinutes() : currentDate.getMinutes());
    xlsx.utils.book_append_sheet(workbook, sheet, sheetName);
    let filename: string = workbook.SheetNames[0].toLowerCase() + '.xlsx';
    xlsx.writeFile(workbook, filename);
  }
}
