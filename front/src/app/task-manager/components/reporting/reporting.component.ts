import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogComponent } from '@shared/components';
import { Task, CategoryTask } from '@shared/models';
import { TaskService } from '@task-manager/services/task.service';
import { ExcelService } from './excel.service';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements AfterViewInit {
  tasks: Task[] = [];
  today: Date = new Date();
  dataSource: MatTableDataSource<any>;
  tableColumns: string[] = ['tagId', 'description', 'categoryTask'];
  categoryTasks: string[] = [];
  filterMessage: string = '';
  baseLength: number = 0;
  clearFiltersDisabled: boolean = true;
  @ViewChild('fromDate')
  fromDate;
  @ViewChild('toDate')
  toDate;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  @ViewChild('categoryTasksSelectionList')
  CategoryTasksSelectionList;

  constructor(
    private dialog: MatDialog,
    private taskService: TaskService,
    private excelService: ExcelService
  ) {}

  ngAfterViewInit() {
    this.taskService.getTasks().subscribe((res: Task[]) => {
      this.tasks = res;
      this.tasks = this.tasks.map((task: any) => {
        if (task.created && task.created !== undefined) {
          task.created = (new Date(task.created.split('T')[0])).getTime();
        } else if (task.updated && task.updated !== undefined) {
          task.created = (new Date(task.updated.split('T')[0])).getTime();
        }
        return task;
      });
      this.tasksToReportTable();
      this.categoryTasksList();
      this.baseLength = this.tasks.length;
    });
  }

  tasksToReportTable() {
    let tableData: any[] = this.tasks.map((task: any) => ({
      tagId: Task.getTagId(task),
      description: (task.description && task.description !== undefined)? task.description : "",
      categoryTask: (task.categoryTask && task.categoryTask.name !== undefined)? task.categoryTask.name : ""
    }));
    this.dataSource = new MatTableDataSource<any>(tableData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  categoryTasksList() {
    this.categoryTasks = [];
    this.tasks.forEach((task: any) => {
      if (task.categoryTask && !this.categoryTasks.includes(task.categoryTask.name)) {
        this.categoryTasks.push(task.categoryTask.name);
      }
    });
  }

  filterByTaskCreationDateRange() {
    let from: number = this.convertUSDateToTimestamp(this.fromDate.nativeElement.value);
    let to: number = this.convertUSDateToTimestamp(this.toDate.nativeElement.value);
    if (!isNaN(from) && !isNaN(to)) {
      if (from > to) {
        DialogComponent.openDialog(this.dialog, `The task created from date can't be greater than task created to date.`);
      } else {
        this.tasks = this.tasks.filter((task: any) => task.created >= from && task.created <= to);
      }
    } else if (!isNaN(from) && isNaN(to)) {
      this.tasks = this.tasks.filter((task: any) => task.created >= from);
    } else if (isNaN(from) && !isNaN(to)) {
      this.tasks = this.tasks.filter((task: any) => task.created <= to);
    }
    this.filterMessage = (this.tasks.length > 0)? String(this.tasks.length) + ' task(s) found' : 'No task found';
    this.tasksToReportTable();
    this.categoryTasksList();
    this.clearFiltersDisabled = false;
  }

  filterByCategoryTasks() {
    let filteredTasks: Task[] = [];
    this.CategoryTasksSelectionList.selectedOptions.selected.forEach((option: any) => {
      filteredTasks.push(...this.tasks.filter((task: any) => task.categoryTask.name === option.value));
    });
    this.tasks = filteredTasks;
    this.filterMessage = (this.tasks.length > 0)? String(this.tasks.length) + ' task(s) found' : 'No task found';
    this.tasksToReportTable();
    this.categoryTasksList();
    this.clearFiltersDisabled = false;
  }

  clearFilters() {
    this.fromDate.nativeElement.value = '';
    this.toDate.nativeElement.value = '';
    this.CategoryTasksSelectionList.selectedOptions.selected.forEach((option: any) => option.selected = false);
    this.filterMessage = '';
    this.ngAfterViewInit();
    this.clearFiltersDisabled = true;
  }

  generateExcelReport() {
    this.excelService.createReport(this.tasks);
  }

  private convertUSDateToTimestamp(date: string): number {
    let dateParts: string[] = date.split('/').map((v: string, i: number) => {
      return ((i === 0 || i === 1) && Number(v) < 10)? '0' + v : v;
    });
    date = dateParts[2] + '-' + dateParts[0] + '-' + dateParts[1];
    return new Date(date).getTime();
  }
}
