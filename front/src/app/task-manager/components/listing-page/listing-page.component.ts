import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { flatMap, tap, map, delay } from 'rxjs/operators';
import { UserService, FloorService, CompanyService, TaskService, CategoryTaskService } from '@task-manager/services';
import { Type, Item, DetailMode, MagicStrings } from '@shared/models';

@Component({
  selector: 'listing-page',
  templateUrl: './listing-page.component.html',
  styleUrls: ['./listing-page.component.scss']
})
export class ListingPageComponent implements OnInit {
  MagicStrings = MagicStrings;
  DetailMode = DetailMode;
  Type = Type;
  @Input()
  type: string = null;
  @Input()
  items: Item[] = [];
  showSpinner: boolean;
  @Input()
  typeListing: string = null;
  @Input()
  titleList: string = null;
  @Input()
  filterByCompanyId: string = null;
  @Input()
  filterByFloorId: string = null;
  @Output()
  refresh: EventEmitter<any> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private floorService: FloorService,
    private companyService: CompanyService,
    private taskService: TaskService,
    private categoryTaskService: CategoryTaskService
  ) {}

  ngOnInit() {
    this.showSpinner = true;
    if (this.typeListing !== MagicStrings.NestedList) {
      this.getItemsFromParams();
    } else {
      if (this.filterByCompanyId) {
        this.floorService.getByCompany(this.filterByCompanyId).subscribe((items: Item[]) => this.setItems(items));
      } else if (this.filterByFloorId) {
        // If is a taskList nested on FloorPageDetail, the items will be set manually from there
        if (this.typeListing !== MagicStrings.NestedList) {
          this.taskService.getByFloor(this.filterByFloorId).subscribe((items: Item[]) => this.setItems(items));
        }
      } else {
        this.getItemsByType(this.type).subscribe((items: Item[]) => this.setItems(items));
      }
    }
  }

  filterByFloor(itemsFilter: any[]) {
    return itemsFilter.filter(item => item.location._id === this.filterByFloorId);
  }

  /** Get the type of the items (Taks|Floor|etc) from the route
   * and retrieve the the list of the targeted elements
   */
  getItemsFromParams() {
    this.route.params
      .pipe(
        delay(500),
        map(params => params.type),
        tap(type => (this.type = type)),
        flatMap(type => this.getItemsByType(type))
      )
      .subscribe((items: Item[]) => this.setItems(items));
  }

  getItemsByType(type: string): any {
    switch (type) {
      case Type.User:
        return this.userService.getUsers();
      case Type.Floor:
        return this.floorService.getFloors();
      case Type.Company:
        return this.companyService.getCompanies();
      case Type.Task:
        return this.taskService.getTasks();
      case Type.CategoryTask:
        return this.categoryTaskService.getCategoryTasks();
    }
  }

  goDetailPage(item: Item = null, mode: string = DetailMode.View) {
    if (item) {
      localStorage.setItem(MagicStrings.DetailItem, JSON.stringify(item));
    }

    // Workaround to ensure that if we are editing a floor, when navigating to one of its tasks, the Floor is updated
    if (this.typeListing === MagicStrings.NestedList) {
      this.refresh.emit();
    }

    this.typeListing = '';
    this.router.navigate([`task-manager/listing/${this.type}/detail`, mode]);
  }

  private disableNewUserButton() {
    if (this.type === Type.User && !this.userService.userIsAdmin()) {
      return true;
    }
    return false;
  }

  setItems(items: Item[]) {
    this.items.splice(0, this.items.length);
    this.items.push(...items);
    this.showSpinner = false;
  }

  getItems(): Item[] {
    return this.items;
  }
}
