import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Type } from '@shared/models';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '@task-manager/services';
import { environment } from '@env/environment';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  Type = Type; // Needed to use the enum in the html
  readonly SMALL_WIDTH_BREAKPOINT = 720;
  private mediaMatcher: MediaQueryList = matchMedia(`(max-width: ${this.SMALL_WIDTH_BREAKPOINT}px)`);
  @ViewChild('sidenav')
  sidenav;
  selectedType: string = 'Work';
  version: string;

  constructor(
    private ngZone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.mediaMatcher.addListener(mql => this.ngZone.run(() => (this.mediaMatcher = <any>mql)));
  }

  ngOnInit() {
    this.version = environment.version;
  }

  logOut() {
    this.userService.logOut();
  }

  isScreenSmall() {
    return this.mediaMatcher.matches;
  }

  clickMenuItem(type: string) {
    this.selectedType = type;
    if (this.isScreenSmall()) {
      this.sidenav.toggle();
    }
    if (type === 'Reporting') {
      this.router.navigate(['reporting/excel'], { relativeTo: this.route });
    } else {
      this.router.navigate(['listing', type], { relativeTo: this.route });
    }
  }
}
