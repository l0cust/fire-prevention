import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { User, userLogedKey } from '@shared/models';
import { UserService } from '@task-manager/services';

@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.scss']
})
export class MainToolbarComponent implements OnInit {
  @Output()
  toggleSidenav = new EventEmitter<void>();
  @Input()
  selectedType: string;

  constructor(public userService: UserService) {}

  ngOnInit() {
    const itemStored = localStorage.getItem(userLogedKey());
    if (itemStored !== 'undefined' && itemStored) {
      // Save the loged user on the user service, loaded from the localstorage
      this.userService.userLoged = new User(JSON.parse(itemStored));
    }
  }
}
