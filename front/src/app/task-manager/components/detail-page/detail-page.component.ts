import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import {
  DetailMode,
  MagicStrings,
  Type,
  Item,
  User,
  Floor,
  Company,
  Task,
  IServerRes,
  CategoryTask
} from '@shared/models';
import { TagIdPipe } from '@shared/pipes';
import { Observable, Subscription, of } from 'rxjs';
import { MatDialog } from '@angular/material';
import { UserService, FloorService, CompanyService, TaskService, CategoryTaskService } from '@task-manager/services';
import { DialogComponent } from '@shared/components/dialog.component';

/**
 * This is a core piece of the app. This component manage the common logic of all the different detail pages.
 * The specific sections of each page are shown depend of the url parameters
 */
@Component({
  selector: 'detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  providers: [TagIdPipe]
})
export class DetailPageComponent implements OnInit, OnDestroy {
  Type = Type;
  DetailMode = DetailMode;
  item: Item;
  mode: string;
  showDelete: boolean = false; // Forces another step to delete an item
  typeItem: string;
  navigationSubscription$: Subscription;

  @ViewChild('userSection')
  userSection;
  @ViewChild('taskSection')
  taskSection;
  @ViewChild('floorSection')
  floorSection;

  forceRefresh: boolean = false; // avoid launch all logic if the params have not changed on paramSubscription
  // store the result of saving the just created task, to be displayed after saving the location of the task
  tempResult: IServerRes;
  initialLocationId: string;
  disabledSaveBtn: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private floorService: FloorService,
    private companyService: CompanyService,
    private taskService: TaskService,
    private categoryTaskService: CategoryTaskService,
    public dialog: MatDialog,
    public tagIdPipe: TagIdPipe
  ) {}

  ngOnDestroy() {
    this.navigationSubscription$.unsubscribe();
  }

  ngOnInit() {
    // If there is any event change on the route, reload the inital logic
    this.navigationSubscription$ = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.init();
      }
    });
    this.init();
  }

  /** Set the inital logic on page start */
  init() {
    this.getItemType();
    this.subscribeRouteParams();
  }

  /**
   * Manage the logic to show one detail page or other.
   * This differentiation is based on the route parameters (ie: /Task/detail/view)
   */
  subscribeRouteParams() {
    this.activatedRoute.params.subscribe(params => {
      // To continue even if the mode of the route has not changed
      // Workaround to avoid double loading on particular cases
      if (this.forceRefresh) {
        this.forceRefresh = false;
      } else if (this.mode === params.mode) {
        return;
      }

      if (params.initialLocation) {
        this.initialLocationId = params.initialLocation;
      }

      if (this.taskSection && this.forceHardRefresh) {
        this.hardRefreshing();
      }

      this.mode = params.mode;
      this.typeItem = params.type;

      switch (params.mode) {
        case DetailMode.Create:
          this.item = this.createByType();
          this.setItemEdit();
        case DetailMode.Edit:
        case DetailMode.View:
          this.getItemEdit(true);
          break;
        default:
          this.changeMode(DetailMode.View);
          break;
      }
    });
  }

  /** Check the url and get the typeItem that we are working in */
  getItemType() {
    const url = this.getUrlSection(-1);
    this.typeItem = Object.values(Type).find(type => Boolean(url.match(new RegExp(type))));
  }

  /**
   * Manage the logic after the edition request has been done
   */
  finishSaving(result: IServerRes = this.tempResult) {
    this.forceHardRefresh = true;
    DialogComponent.openDialog(this.dialog, `${this.tagIdPipe.transform(result.data)}: ${result.message}`);
    this.setItemEdit(result.data);
    this.getItemEdit();
    this.changeMode(DetailMode.View);
  }

  save() {
    if (this.disabledSaveBtn) {
      return;
    }
    this.preventMultipleClick();

    // Manage the response of the server of the different detail pages. Special focus on complicated cases
    this.saveByType().subscribe(
      (result: IServerRes) => {
        if (result.ok) {
          if (this.typeItem === Type.Task && (<Task>this.item).location) {
            this.taskSection.saveLocationAndImages((<Task>this.item).location._id, result.data._id);
            this.tempResult = result;
          } else if (this.typeItem === Type.Floor) {
            this.floorSection.saveImage(result.data._id);
            this.tempResult = result;
          } else {
            this.finishSaving(result);
          }
        } else {
          this.handleError(result, result.message);
        }
      },
      error => this.handleError(error, error.message || error.error.message)
    );
  }

  delete() {
    DialogComponent.openDialog(
      this.dialog,
      `Are you sure that you want to delete ${this.tagIdPipe.transform(this.item)}`,
      true
    ).subscribe(result => {
      if (result) {
        this.deleteByType().subscribe(
          (result: IServerRes) => {
            if (result.ok) {
              DialogComponent.openDialog(this.dialog, `${this.tagIdPipe.transform(result.data)}:  ${result.message}`);
              this.item = null;
              this.setItemEdit();
              this.goToListingPage();
            } else {
              this.handleError(result, result.message);
            }
          },
          error => this.handleError(error, error.message || error.error.message)
        );
      } else {
        this.showDelete = false;
      }
    });
  }

  /** Create an object of the type that is being handled (type of Detail Page)  */
  createByType(json?: any): Item {
    switch (this.typeItem) {
      case Type.User:
        return new User(json);
      case Type.Floor:
        return new Floor(json);
      case Type.Company:
        return new Company(json);
      case Type.Task:
        return new Task(json);
      case Type.CategoryTask:
        return new CategoryTask(json);
    }
  }

  /**
   * To keep all the saving logic together, this function returns:
   * The function depending on the mode (create|edit|delete)
   * of the service depending on the type (Task|Floor|etc)
   * In that way, we manage multiple services, and multiple operations, on one function
   */
  saveByType(): Observable<IServerRes | boolean> {
    switch (this.typeItem) {
      case Type.User:
        if (this.mode === DetailMode.Create) {
          const verifyPassword = this.userSection.getPassword();
          if (verifyPassword.ok) {
            this.item['password'] = verifyPassword.password;
          } else {
            return of(verifyPassword);
          }
        }
        return this.userService[this.mode](<User>this.item);
      case Type.Floor:
        return this.floorService[this.mode](<Floor>this.item);
      case Type.Company:
        return this.companyService[this.mode](<Company>this.item);
      case Type.Task:
        return this.taskService[this.mode](<Task>this.item, this.taskSection.images);
      case Type.CategoryTask:
        return this.categoryTaskService[this.mode](<CategoryTask>this.item);
    }
  }

  deleteByType(): Observable<IServerRes | boolean> {
    switch (this.typeItem) {
      case Type.User:
        return this.userService.delete(<User>this.item);
      case Type.Floor:
        return this.floorService.delete(<Floor>this.item);
      case Type.Company:
        return this.companyService.delete(<Company>this.item);
      case Type.Task:
        return this.taskService.delete(<Task>this.item);
      case Type.CategoryTask:
        return this.categoryTaskService.delete(<CategoryTask>this.item);
    }
  }

  /** Get the current url segments, and build & natigate to a new one toggling the last param for the new one */
  changeMode(mode: string) {
    this.router.navigate([`task-manager/${this.getUrlSection(-1)}`, mode]);
  }

  /** Return the current url avoiding nRemove latest sections of the url */
  getUrlSection(nRemove: number) {
    return this.activatedRoute.snapshot.url
      .map(segment => segment.path)
      .slice(0, nRemove)
      .join('/');
  }

  goToListingPage() {
    this.router.navigate([`task-manager/${this.getUrlSection(-2)}`]);
  }

  /**
   * If this.item is empty, fill it with the stored item
   * @param force true to overwrite the current item with the stored one
   */
  getItemEdit(force: boolean = false) {
    if (!this.item || force) {
      const itemStored = localStorage.getItem(MagicStrings.DetailItem);
      if (itemStored !== 'undefined' && itemStored) {
        this.item = this.createByType(JSON.parse(itemStored));
      } else {
        this.item = this.createByType();
      }
    }
  }

  /** @param data item saved returned by the database */
  setItemEdit(data: any = null) {
    if (this.item) {
      if (data) {
        this.item = this.createByType(data);
        localStorage.setItem(MagicStrings.DetailItem, JSON.stringify(data));
      } else {
        localStorage.setItem(MagicStrings.DetailItem, JSON.stringify(this.item));
      }
      if (this.item._id === this.userService.userLoged._id) {
        this.userService.setUserLoged(<User>this.item);
      }
    } else {
      localStorage.removeItem(MagicStrings.DetailItem);
    }
  }

  cancelEditMode() {
    DialogComponent.openDialog(
      this.dialog,
      (this.mode === DetailMode.Create)? `Are you sure that you want to cancel the ${this.typeItem} creation? You will loose unsaved changes.` : `Are you sure that you want to cancel the ${this.typeItem} edition? You will loose unsaved changes.`,
      true
    ).subscribe(result => {
      if (result) {
        if (this.taskSection) {
          this.taskSection.cancelEdition();
        }
        this.getItemEdit(true);
        (this.mode === DetailMode.Create)? this.goToListingPage() : this.changeMode(DetailMode.View);
      }
    });
  }

  private handleError(error: any, message: string) {
    console.error('error', error);
    DialogComponent.openDialog(this.dialog, message);
  }

  forceHardRefresh: boolean = false; // to manage hardRefreshing only when we want and not with each params change
  hardRefreshFlag: boolean = true; // show and hide the target elements in the dom
  /** Remove and add from the DOM elements (taskSection)
   * to force ngAfterViewInit and load images properly after creating a task with images and location */
  hardRefreshing() {
    this.hardRefreshFlag = false;
    this.forceHardRefresh = false;
    setTimeout(() => {
      this.hardRefreshFlag = true;
    }, 10);
  }

  private disableButtonUserButton() {
    if (this.typeItem === Type.User && !this.userService.userIsAdmin()) {
      return true;
    }
    return false;
  }

  refresh() {
    this.forceRefresh = true;
  }

  preventMultipleClick() {
    this.disabledSaveBtn = true;
    setTimeout(() => (this.disabledSaveBtn = false), 500);
  }
}
