import { Component, Input } from '@angular/core';
import { Company, DetailMode, MagicStrings, Type } from '@shared/models';

@Component({
  selector: 'detail-company',
  templateUrl: './detail-company.component.html',
  styleUrls: ['./detail-company.component.scss']
})
export class DetailCompanyComponent {
  Type = Type;
  MagicStrings = MagicStrings;
  DetailMode = DetailMode;
  @Input('item')
  company: Company;
  @Input()
  mode: string;
}
