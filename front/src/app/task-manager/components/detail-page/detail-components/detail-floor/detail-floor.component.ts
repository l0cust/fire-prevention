import { Component, Input, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CompanyService, ImageService, TaskService, CategoryTaskService } from '@task-manager/services';
import { Router } from '@angular/router';
import {
  Floor,
  DetailMode,
  Company,
  Type,
  MagicStrings,
  SectionsImage,
  ImageFire,
  Task,
  CategoryTask
} from '@shared/models';
import { filterByTaskCategory } from '@shared/utils';
import { tap } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'detail-floor',
  templateUrl: './detail-floor.component.html',
  styleUrls: ['./detail-floor.component.scss']
})
export class DetailFloorComponent implements OnInit {
  SectionsImage = SectionsImage;
  MagicStrings = MagicStrings;
  Type = Type;
  DetailMode = DetailMode;
  @Input('item')
  floor: Floor;
  @Input()
  mode: string;
  companies: Company[];

  @Output()
  refresh: EventEmitter<any> = new EventEmitter();
  @Output()
  locationSavedTemp: EventEmitter<any> = new EventEmitter();

  @ViewChild('imageLocationRelatedTask')
  imageLocationRelatedTask;
  @ViewChild('imageSectionLocation')
  imageSectionLocation;
  @ViewChild('listingPage')
  listingPage;

  subElementRelatedAvailable: boolean = false;
  categoriesTask: CategoryTask[];
  relatedTasks: Task[];
  relatedTasksFiltered: Task[] = [];

  constructor(
    private companyService: CompanyService,
    private imageService: ImageService,
    private taskService: TaskService,
    private categoryTaskService: CategoryTaskService,
    private router: Router
  ) {
    this.companyService.getCompanies().subscribe(companies => (this.companies = companies));
  }

  ngOnInit() {
    forkJoin(
      this.categoryTaskService.getCategoryTasks().pipe(tap(res => (this.categoriesTask = res))),
      this.getImages()
    ).subscribe();
  }

  /**
   * Compare value from selection with values from option list
   * @param a value to compare from select option list
   * @param b value to compare from the selection (floor.company)
   */
  compSt(a, b) {
    return a && b && a._id === b._id;
  }

  getRelatedTasks() {
    this.taskService.getByFloor(this.floor._id).subscribe((res: any[]) => {
      this.relatedTasks = res.map(t => new Task(t));
      this.filterTaskCategoryChange([]);
    });
  }

  imageLocationFloor: ImageFire;
  /** Get the images related to the floor (should be only one) */
  getImages(): Observable<ImageFire[]> {
    let listedImages: any[];
    if (this.floor && this.floor._id) {
      return this.imageService.listImages(`${Type.Floor}/${this.floor._id}`).pipe(
        tap((res: ImageFire[]) => {
          listedImages = res;
          listedImages.forEach((image: ImageFire) => {
            this.imageService.getImage(image.path, image.name).subscribe((res: Blob) => {
              image.setImageObjAndNotifiy(res).subscribe(resNotification => {
                this.imageLocationFloor = image;
                this.getRelatedTasks();
              });
            });
          });
        })
      );
    }
  }

  /** Save the image on the Floor. The server handles the removal of the posible previous images
   * to keep just one image for each floor
   */
  saveImage(floorId: string) {
    this.imageSectionLocation.removePreviousImagesAndUpload(floorId, false).subscribe(res => {
      this.locationSavedTemp.emit();
      this.getImages().subscribe();
    });
  }

  /**
   * Manage the event of a marker being clicked
   * @param taskClicked task of the clicked marker
   */
  markerLocationClicked(taskClicked: Task) {
    localStorage.setItem(MagicStrings.DetailItem, JSON.stringify(taskClicked));
    this.refresh.emit();
    this.goDetailPage(taskClicked);
  }

  goDetailPage(task: Task, mode: string = DetailMode.View) {
    localStorage.setItem(MagicStrings.DetailItem, JSON.stringify(task));
    this.router.navigate([`task-manager/listing/${Type.Task}/detail`, mode]);
  }

  createNewTask() {
    this.router.navigate([
      `task-manager/listing/${Type.Task}/detail`,
      DetailMode.Create,
      { initialLocation: this.floor._id }
    ]);
  }

  /**
   * Filter tasks by taskCategory and then show those tasks on the mapFloor and nested list
   * @param selectedTypeTasks type of the task to filter by
   */
  filterTaskCategoryChange(selectedTypeTasks: CategoryTask[]) {
    const arrFiltered: Task[] = filterByTaskCategory(selectedTypeTasks, this.relatedTasks);
    this.listingPage && this.listingPage.setItems(arrFiltered);
    this.drawMarkers(arrFiltered);
  }

  /** If the floor has related tasks, draw the position of each task as a marker */
  drawMarkers(tasksToDraw: Task[]) {
    if (this.imageLocationRelatedTask) {
      this.imageLocationRelatedTask.drawRelatedTasks(tasksToDraw);
    } else {
      console.error('No imageLocationRelatedTask defined');
    }
  }

  /** Quick export to PFD */
  export() {
    if (this.listingPage) {
      try {
        const relatedTask: Task[] = this.listingPage.getItems();
        const doc = new jsPDF();
        let myText: string = `Tasks related to ${this.floor.name} \n\n\n`;
        relatedTask.forEach(t => {
          myText += `${t.getTagId()}\n`;
        });
        doc.text(myText, 10, 10);

        doc.save('Export.pdf');
      } catch (err) {
        alert('Error doing the export');
      }
    }
  }
}
