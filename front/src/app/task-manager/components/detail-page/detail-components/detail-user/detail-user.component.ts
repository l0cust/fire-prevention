import { Component, Input, OnInit } from '@angular/core';
import { User, DetailMode, Roles } from '@shared/models';
import { UserService } from '@task-manager/services/user.service';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '@shared/components/dialog.component';
import { get } from 'lodash';

@Component({
  selector: 'detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {
  DetailMode = DetailMode;
  roles: string[];
  editingOwnPassword: boolean = false;

  password1: string = '';
  password2: string = '';

  @Input('item')
  user: User;
  @Input()
  mode: string;

  constructor(private userService: UserService, public dialog: MatDialog) {}

  ngOnInit() {
    this.roles = Object.values(Roles);
  }

  public getPassword(): { ok: boolean; password?: string; message?: string } {
    if (this.password1 !== this.password2) {
      return { ok: false, message: 'Both password fields have to match' };
    }
    if (this.password1 === '') {
      return { ok: false, message: 'Please fill the password' };
    }

    return { ok: true, password: this.password1 };
  }

  showChangePassword(): boolean {
    return (this.userService.userLoged.isAdmin() && this.mode === DetailMode.Edit) || this.editingOwnPassword;
  }

  changePassword() {
    const result = this.getPassword();
    if (!result.ok) {
      DialogComponent.openDialog(this.dialog, result.message);
    } else {
      this.userService.updatePassword(this.user._id, this.password1).subscribe(
        res => {
          if (!res.ok || res.error) {
            DialogComponent.openDialog(this.dialog, res.error.message);
          } else {
            DialogComponent.openDialog(this.dialog, 'Password updated');
          }
        },
        err => DialogComponent.openDialog(this.dialog, 'Error: ' + get(err, 'error.message'))
      );
    }
  }

  showEditOwnPasswordButton(): boolean {
    return !this.userService.userLoged.isAdmin() && this.isOwnUser();
  }

  /** @return true if user loged is viewing its own details */
  isOwnUser(): boolean {
    return this.user._id === this.userService.userLoged._id;
  }

  private disableRole() {
    return this.mode === DetailMode.View || !this.userService.userIsAdmin();
  }

  private tooltipRole() {
    if (this.mode === DetailMode.View) {
      return '';
    }
    if (!this.userService.userIsAdmin()) {
      return 'You need to be admin to edit roles';
    }
    return '';
  }
}
