import { Component, Input } from '@angular/core';
import { CategoryTask, DetailMode, MagicStrings } from '@shared/models';

@Component({
  selector: 'detail-category-task',
  templateUrl: './detail-category-task.component.html',
  styleUrls: ['./detail-category-task.component.scss']
})
export class DetailCategoryTaskComponent {
  MagicStrings = MagicStrings;
  DetailMode = DetailMode;
  CategoryTask = CategoryTask;
  @Input('item')
  categoryTask: CategoryTask;
  @Input()
  mode: string;

  addType: boolean = false;
  addMeassure: boolean = false;
  addAmount: boolean = false;
}
