import { Component, Input, AfterViewInit, ViewChild, Output, EventEmitter } from '@angular/core';
import {
  Task,
  DetailMode,
  User,
  Floor,
  IImageToUpload,
  Type,
  ImageFire,
  SectionsImage,
  CategoryTask
} from '@shared/models';
import { UserService, FloorService, ImageService, CategoryTaskService } from '@task-manager/services';
import { DialogComponent } from '@shared/components/dialog.component';
import { MatDialog } from '@angular/material';
import { Subscription, Observable, forkJoin } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';

@Component({
  selector: 'detail-task',
  templateUrl: './detail-task.component.html',
  styleUrls: ['./detail-task.component.scss']
})
export class DetailTaskComponent implements AfterViewInit {
  CategoryTask = CategoryTask;
  Type = Type;
  DetailMode = DetailMode;
  SectionsImage = SectionsImage;
  @Input('item')
  task: Task;
  @Input()
  mode: string;
  users: User[];
  floors: Floor[];
  images: IImageToUpload[] = [];
  listedImages: ImageFire[] = [];
  categoryTasks: CategoryTask[];

  imageLocation: ImageFire;

  subscriptions: Subscription[] = [];
  showEditLocation: boolean = false;
  @ViewChild('imageLocationEditor')
  imageLocationEditor;

  @Input()
  initialLocationId: string = '';
  @Output()
  locationSavedTemp: EventEmitter<any> = new EventEmitter();

  @ViewChild('imageSectionBefore')
  imageSectionBefore;
  @ViewChild('imageSectionAfter')
  imageSectionAfter;

  showSpinner: boolean = false;

  constructor(
    private userService: UserService,
    private floorService: FloorService,
    private imageService: ImageService,
    private categoryTaskService: CategoryTaskService,
    public dialog: MatDialog
  ) {
    this.userService.getUsers().subscribe(users => (this.users = users));
    this.floorService.getFloors().subscribe(floors => {
      this.floors = floors;

      // If the task has a location from the begining
      if (this.initialLocationId) {
        this.task.location = this.floors.find(l => l._id === this.initialLocationId);
        this.loadLocation(); // to trigger the image loading
      }
    });
    this.categoryTaskService.getCategoryTasks().subscribe(categoryTasks => (this.categoryTasks = categoryTasks));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  categoryTaskSelectionChange() {
    if (this.imageLocationEditor) {
      this.imageLocationEditor.refreshMarker();
    }
  }

  ngAfterViewInit() {
    this.task.location && this.getImageLocation(this.task.location._id, Type.Floor);
  }

  initLocationEdition() {
    this.showEditLocation = true;
  }

  /** If the user cancel the changes without saving them */
  cancelEdition() {
    this.imageLocationEditor.removePreviousMarkers();
    this.task.positionMarker = this.imageLocationEditor.originalPositionMarker;
    this.imageLocationEditor.buildHtmlMarker(this.task);
    this.showEditLocation = false;
  }

  restoreDefaultLocation() {
    this.locationChanged(`Are you sure about restoring a clean version of the location?
        You will loose all the edition marks.`);
  }

  /**
   * Get the imageKeys of the images related to itemId
   * If there are keys, load those images
   * Show the image and notify that the image has already been loaded
   * 
   * @param itemId id of the element to retrieve its images
   * @param type type of the element to retrieve its images
   * @param showEdit
   */
  getImageLocation(itemId: string, type: string, showEdit: boolean = false) {
    this.imageService.listImages(`${type}/${itemId}`).subscribe((res: ImageFire[]) => {
      this.listedImages = res;
      if (this.listedImages && this.listedImages.length > 0) {
        this.imageLocationEditor.startLoading();
        this.imageService.getImage(this.listedImages[0].path, this.listedImages[0].name).subscribe(async (res: Blob) => {
          this.imageLocation = new ImageFire({ name: this.listedImages[0].name, path: this.listedImages[0].path });
          this.subscriptions.push(
            this.imageLocation.setImageObjAndNotifiy(res).subscribe((loaded: boolean) => {
              if (loaded) {
                this.showEditLocation = showEdit;
              }
              this.imageLocationEditor.stopLoading();
            })
          );
        });
      }
    });
  }

  loadLocation() {
    if (this.task.location) {
      this.getImageLocation(this.task.location._id, Type.Floor, true);
    } else {
      console.error('No location defined');
    }
  }

  locationChanged(textToDisplay?: string) {
    if (this.mode === DetailMode.Create) {
      this.loadLocation();
    } else {
      const textChangeLocation: string = `Are you sure about changing the location assigned to this task?
            You will loose the previous location edition.`;
      DialogComponent.openDialog(this.dialog, textToDisplay || textChangeLocation, true).subscribe(result => {
        if (result) {
          this.showSpinner = true;
          this.task.location &&
            this.saveChangeLocation(this.task.location._id, this.task._id)
              .pipe(finalize(() => (this.showSpinner = false)))
              .subscribe();
        }
      });
    }
  }

  saveChangeLocation(locationId: string, taskId: string): Observable<any> {
    return this.imageService.changeLocation(locationId, taskId).pipe(
      tap((res: Blob) => {
        this.imageLocation = new ImageFire({ name: 'fasdf' });
        this.imageLocation.setImageObj(res);
        this.showEditLocation = false;
      })
    );
  }

  saveLocationAndImages(locationId: string, taskId: string) {
    forkJoin(
      this.saveChangeLocation(locationId, taskId),
      this.imageSectionBefore.uploadImages(taskId, false),
      this.imageSectionAfter.uploadImages(taskId, false)
    ).subscribe(res => {
      this.locationSavedTemp.emit();
    });
  }

  convertMeasure() {
    this.task.measure = (this.task.positionMarker.width * this.task.positionMarker.height) / 10000;
  }

  /**
   * Compare value from selection with values from option list
   * @param a value to compare from select option list
   * @param b value to compare from the selection
   */
  compSt(a, b) {
    return a && b && a._id === b._id;
  }

  private handleError(error: any, message: string) {
    console.error('error', error);
    DialogComponent.openDialog(this.dialog, message);
  }
}
