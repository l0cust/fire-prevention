export { DetailCompanyComponent } from './detail-company/detail-company.component';
export { DetailFloorComponent } from './detail-floor/detail-floor.component';
export { DetailTaskComponent } from './detail-task/detail-task.component';
export { DetailUserComponent } from './detail-user/detail-user.component';
export { DetailCategoryTaskComponent } from './detail-category-task/detail-category-task.component';
