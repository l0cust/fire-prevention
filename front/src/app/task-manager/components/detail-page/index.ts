export { DetailPageComponent } from './detail-page.component';
export {
  DetailCompanyComponent,
  DetailFloorComponent,
  DetailTaskComponent,
  DetailUserComponent,
  DetailCategoryTaskComponent
} from './detail-components';
