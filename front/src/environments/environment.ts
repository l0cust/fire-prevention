export const environment = {
  production: false,
  URL: 'http://localhost:3000/api',
  version: require('./../../package.json').version
};
